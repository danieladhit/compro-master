<div id="content-wrapper">

        <div class="page-header">
            <h1><span class="text-light-gray"><a href="<?php echo site_url('admin/dashboard')?>"><?php echo ucwords('dashboard') ?></a> / </span><?php echo ucwords($heading)?></h1>
        </div> <!-- / .page-header -->



        <?php $this->load->view('admin/field/flash_message'); ?>


        <form action="#" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
            <div class="panel-heading">
                <span class="panel-title"><?php echo set_value('heading', isset($heading) ? ucwords($heading) : ''); ?></span>
            </div>
            <div class="panel-body no-padding-hr">

				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">

                    <div class="row">

                        <label class="col-sm-2 control-label">Logo Website</label>

                        <div class="col-sm-8">

                            <form id="uploadimage" action="" method="post" enctype="multipart/form-data">

                                <!-- <h4 id='loading' >loading..</h4>

                                <div id="message"></div>

                                <hr id="line"> -->

                                <br/>

                                <div id="selectImage">

                                    <label>Select Your Image</label><br/>

                                

                                    <input type="file" name="userfile" id="file" required /><br>

                                    <img src="<?php echo base_url() ?>uploads/logo/<?php echo set_value('logo', isset($default['logo']) && $default['logo'] != '' ? $default['logo'] : ''); ?>" width="200">

                                    <div class="clear"></div>

                                    <br/>

                                    <button type="submit" class="btn btn-success submit" >Upload</button>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>
			
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Nama Website</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/website_name/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('website_name', isset($default['website_name']) ? $default['website_name'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Deskripsi Website</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/website_description/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('website_description', isset($default['website_description']) ? $default['website_description'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Website Color</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/website_color/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('website_color', isset($default['website_color']) ? $default['website_color'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <!--<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Keyword Website</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/website_keywords/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('website_keywords', isset($default['website_keywords']) ? $default['website_keywords'] : ''); ?></a>
                        </div>
                    </div>
                </div>-->
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Facebook</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/facebook/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('facebook', isset($default['facebook']) ? $default['facebook'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Google</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/google/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('google', isset($default['google']) ? $default['google'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Twitter</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/twitter/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('twitter', isset($default['twitter']) ? $default['twitter'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Google Map</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/gmap/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('gmap', isset($default['gmap']) ? $default['gmap'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Analytics</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/analytics/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('analytics', isset($default['analytics']) ? $default['analytics'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Chat</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/chat/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('chat', isset($default['chat']) ? $default['chat'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Footer About Us</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/footer_about_us/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('footer_about_us', isset($default['footer_about_us']) ? $default['footer_about_us'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Footer Address</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/footer_address/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('footer_address', isset($default['footer_address']) ? $default['footer_address'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Footer Phone</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/footer_telephone/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('footer_telephone', isset($default['footer_telephone']) ? $default['footer_telephone'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Footer Email</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/footer_email/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('footer_email', isset($default['footer_email']) ? $default['footer_email'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <label class="col-sm-2 control-label">Copyright</label>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="textarea" data-inputclass="some_class" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/copyright/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('copyright', isset($default['copyright']) ? $default['copyright'] : ''); ?></a>
                            <style type="text/css">.some_class { width: 260px !important; }</style>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
						<?php if($template_list_active->num_rows() > 0){ 
							$template_name = $template_list_active->row()->template_name;
						}
						else
						{
							$template_name = "";
						}
						?>
					 	<label class="col-sm-2 control-label">Template</label>
                        <div class="col-sm-8">
                            <a href="#" 
                                class="form_editable" 
                                <?php
                                if( count($template_list->result()) > 0 ): ?>
                                <?php 
                                    $data_source = '';
                                    $p = 1;
                                    foreach( $template_list->result() as $tl ){
                                        $separator = ',';
                                        if( count($template_list->result()) == $p ){
                                            $separator = '';   
                                        }
                                        $data_source .= "{value: '".$tl->template_id."',text: '".$tl->template_name."'}".$separator;
                                        $p++;
                                    }
                                ?>
                                <?php endif; ?>
                                data-source="[<?php echo $data_source ?>]" 
                                data-type="select" 
                                data-pk="1" 
                                data-mode="inline"  
                                data-title="Ubah data"  
                                data-url="<?php echo base_url()?>admin/setting/update_themes/"><?php echo $template_name; ?></a>
                        </div>
					</div>
                </div>
            </div>
        </form>

		<form action="#" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
            <div class="panel-heading">
                <span class="panel-title">Setting Home Content</span>
            </div>
            <div class="panel-body no-padding-hr">
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Klien
                        </div>
                        <div class="col-sm-2">
                            <a href="#" class="form_editable" data-source="[{value: 'ya', text: 'Aktif'},{value: 'tidak', text: 'Tidak Aktif'}]" data-type="select" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/klien/1/setting_home_id/setting_home"><?php echo set_value('setting_home_list', $setting_home_list->num_rows() > 0 && $setting_home_list->row()->klien == 'ya' ? 'Aktif' : 'Tidak Aktif'); ?></a>
                        </div>
						<div class="col-sm-2">
							<a href="<?php echo base_url()?>admin/klien" class="btn btn-success">Edit</a>
						</div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Berita
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-source="[{value: 'ya', text: 'Aktif'},{value: 'tidak', text: 'Tidak Aktif'}]" data-type="select" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/berita/1/setting_home_id/setting_home"><?php echo set_value('setting_home_list', $setting_home_list->num_rows() > 0 && $setting_home_list->row()->berita == 'ya' ? 'Aktif' : 'Tidak Aktif'); ?></a>
                        </div>
                    </div>
                </div>
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Blog
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-source="[{value: 'ya', text: 'Aktif'},{value: 'tidak', text: 'Tidak Aktif'}]" data-type="select" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/blog/1/setting_home_id/setting_home"><?php echo set_value('setting_home_list', $setting_home_list->num_rows() > 0 && $setting_home_list->row()->blog == 'ya' ? 'Aktif' : 'Tidak Aktif'); ?></a>
                        </div>
                    </div>
                </div>
				
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Galeri
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-source="[{value: 'ya', text: 'Aktif'},{value: 'tidak', text: 'Tidak Aktif'}]" data-type="select" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/gallery/1/setting_home_id/setting_home"><?php echo set_value('setting_home_list', $setting_home_list->num_rows() > 0 && $setting_home_list->row()->blog == 'ya' ? 'Aktif' : 'Tidak Aktif'); ?></a>
                        </div>
                    </div>
                </div>

                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Tambahan
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" 
                            data-source="[{value: 'ya', text: 'Aktif'},{value: 'tidak', text: 'Tidak Aktif'}]" 
                            data-type="select" 
                            data-pk="1" 
                            data-mode="inline"  
                            data-title="Ubah data"  
                            data-url="<?php echo base_url()?>admin/setting/update_field/tambahan/1/setting_home_id/setting_home">
                            <?php echo set_value('setting_home_list', $setting_home_list->num_rows() > 0 && $setting_home_list->row()->tambahan == 'ya' ? 'Aktif' : 'Tidak Aktif'); ?>
                                
                            </a>
                        </div>
                    </div>
                </div>

                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Halaman Tambahan
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control" id="halaman3" name="halaman">
                                <option value="">-- Pilih Halaman Tambahan --</option>
                                <?php foreach($list_page->result() as $row){ ?>
                                    <option value="<?php echo $row->id_page?>"><?php echo $row->page_title?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" type="button" id="buttonTambahHalaman3" onclick="tambahPage3()" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sedang Ditambahkan">Tambahkan</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4" id="list-halaman3">
                            <?php foreach($halaman_tambahan->result() as $row){ ?>
                                <br>
                                <div class="row" id="halaman-selected3-<?php echo $row->page_id?>">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="<?php echo $row->page_title?>" disabled/> 
                                    </div>
                                    <div class="col-md-2">  
                                        <button type="button" class="btn btn-danger" onclick="hapusHalaman3('<?php echo $row->page_id?>')">Hapus</button>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>
                </div>
				
				<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Halaman Our Product
                        </div>
                        <div class="col-sm-4">
							<select class="form-control" id="halaman" name="halaman">
								<option value="">-- Pilih Halaman --</option>
								<?php foreach($list_page->result() as $row){ ?>
									<option value="<?php echo $row->id_page?>"><?php echo $row->page_title?></option>
								<?php } ?>
							</select>
                        </div>
						<div class="col-sm-4">
							<button class="btn btn-success" type="button" id="buttonTambahHalaman" onclick="tambahPage()" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sedang Ditambahkan">Tambahkan</button>
						</div>
                    </div>
					<div class="row">
						<div class="col-sm-8 col-sm-offset-4" id="list-halaman">
							<?php foreach($our_product->result() as $row){ ?>
								<br>
								<div class="row" id="halaman-selected-<?php echo $row->page_id?>">
									<div class="col-md-6">
										<input type="text" class="form-control" value="<?php echo $row->page_title?>" disabled/> 
									</div>
									<div class="col-md-2">	
										<button type="button" class="btn btn-danger" onclick="hapusHalaman('<?php echo $row->page_id?>')">Hapus</button>
									</div>
								</div>
								
							<?php } ?>
						</div>
					</div>
                </div>
				
            </div>
        </form>

        <form action="#" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
            <div class="panel-heading">
                <span class="panel-title">Setting Popular Content</span>
            </div>
            <div class="panel-body no-padding-hr">
               
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Nama Content
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/website_popular/<?php echo set_value('setting_id', isset($default['setting_id']) ? $default['setting_id'] : ''); ?>/setting_id/setting"><?php echo set_value('website_popular', isset($default['website_popular']) ? $default['website_popular'] : ''); ?></a>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                           Halaman Popular Content
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control" id="halaman2" name="halaman">
                                <option value="">-- Pilih Halaman --</option>
                                <?php foreach($list_page_popular->result() as $row){ ?>
                                    <option value="<?php echo $row->id_page?>"><?php echo $row->page_title?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" type="button" id="buttonTambahHalaman2" onclick="tambahPage2()" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sedang Ditambahkan">Tambahkan</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4" id="list-halaman2">
                            <?php foreach($our_product_popular->result() as $row){ ?>
                                <br>
                                <div class="row" id="halaman-selected2-<?php echo $row->page_id?>">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="<?php echo $row->page_title?>" disabled/> 
                                    </div>
                                    <div class="col-md-2">  
                                        <button type="button" class="btn btn-danger" onclick="hapusHalaman2('<?php echo $row->page_id?>')">Hapus</button>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
		
        <?php 
		if( 1 == 2) :
		//if($feature_settings_list->num_rows() > 0) : ?>
        <form action="#" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
            <div class="panel-heading">
                <span class="panel-title">Setting Fitur</span>
            </div>
            <div class="panel-body no-padding-hr">
            <?php foreach($feature_settings_list->result_array() as $row) : ?>
                <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="#" class="form_editable" data-type="text" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/feature_name/<?php echo set_value('feature_settings_id', isset($row['feature_settings_id']) ? $row['feature_settings_id'] : ''); ?>/feature_settings_id/feature_settings">
                                <?php echo set_value('feature_name', isset($row['feature_name']) ? $row['feature_name'] : ''); ?>
                            </a>
                        </div>
                        <div class="col-sm-8">
                            <a href="#" class="form_editable" data-source="[{value: 'yes', text: 'Aktif'},{value: 'no', text: 'Tidak Aktif'}]" data-type="select" data-pk="1" data-mode="inline"  data-title="Ubah data"  data-url="<?php echo base_url()?>admin/setting/update_field/feature_value/<?php echo set_value('feature_settings_id', isset($row['feature_settings_id']) ? $row['feature_settings_id'] : ''); ?>/feature_settings_id/feature_settings"><?php echo set_value('feature_value', isset($row['feature_value']) && $row['feature_value'] == 'yes' ? 'Aktif' : 'Tidak Aktif'); ?></a>
                        </div>
                    </div>
                </div>                
                <?php endforeach; ?>
            </div>
        </form>
        <?php endif; ?>

<!-- /5. $SUMMERNOTE_WYSIWYG_EDITOR -->


    </div> <!-- / #content-wrapper -->
    <div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
    <script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url()?>assets/general/js/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
    <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->

<script src="<?php echo base_url()?>assets/general/js/jquery.transit.js"></script>

<!-- Pixel Admin's javascripts -->
<script src="<?php echo base_url()?>assets/general/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/pixel-admin.min.js"></script>

<script type="text/javascript">

    function tambahPage()
	{
		var pageId	= $("#halaman").val();
		
		$.ajax({
			url	: "<?php echo base_url()?>admin/setting/add_halaman",
			type: "POST",
			data: { page_id : pageId },
			beforeSend : function()
			{
				$("#buttonTambahHalaman").button("loading");
			},
			success : function(response)
			{
				$("#halaman").html(response.list_page);
				$("#list-halaman").append(response.fresh_add);
				$("#buttonTambahHalaman").button("reset");
			}
		});
	}
	
	function hapusHalaman(id)
	{
		$.ajax({
			url	: "<?php echo base_url()?>admin/setting/hapus_halaman",
			type: "POST",
			data: { page_id : id },
			beforeSend : function()
			{
			},
			success : function(response)
			{
				$("#halaman").html(response.list_page);
				$("#halaman-selected-"+id).remove();
			}
		});
	}

    function tambahPage2()
    {
        var pageId  = $("#halaman2").val();
        
        $.ajax({
            url : "<?php echo base_url()?>admin/setting/add_halaman2",
            type: "POST",
            data: { page_id : pageId },
            beforeSend : function()
            {
                $("#buttonTambahHalaman2").button("loading");
            },
            success : function(response)
            {
                $("#halaman2").html(response.list_page);
                $("#list-halaman2").append(response.fresh_add);
                $("#buttonTambahHalaman2").button("reset");
            }
        });
    }
    
    function hapusHalaman2(id)
    {
        $.ajax({
            url : "<?php echo base_url()?>admin/setting/hapus_halaman2",
            type: "POST",
            data: { page_id : id },
            beforeSend : function()
            {
            },
            success : function(response)
            {
                $("#halaman2").html(response.list_page);
                $("#halaman-selected2-"+id).remove();
            }
        });
    }

    function tambahPage3()
    {
        var pageId  = $("#halaman3").val();
        
        $.ajax({
            url : "<?php echo base_url()?>admin/setting/add_halaman3",
            type: "POST",
            data: { page_id : pageId },
            beforeSend : function()
            {
                $("#buttonTambahHalaman3").button("loading");
            },
            success : function(response)
            {
                $("#halaman3").html(response.list_page);
                $("#list-halaman3").append(response.fresh_add);
                $("#buttonTambahHalaman3").button("reset");
            }
        });
    }
    
    function hapusHalaman3(id)
    {
        $.ajax({
            url : "<?php echo base_url()?>admin/setting/hapus_halaman3",
            type: "POST",
            data: { page_id : id },
            beforeSend : function()
            {
            },
            success : function(response)
            {
                $("#halaman3").html(response.list_page);
                $("#halaman-selected3-"+id).remove();
            }
        });
    }
	
	init.push(function () {
        // Javascript code here
        $('.form_editable').editable();
    });
    window.PixelAdmin.start(init);
</script>