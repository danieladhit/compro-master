<div class="custom_content clearfix">
	<div class="container">
		<div class="photo_gallery custom">
			<ul class="gallery popup-gallery gallery-4col">
				<?php foreach($images as $key => $row){ ?>
				<li>
					<a class="popup-gmaps" href="<?=get_iframe_src($row->galeri_description) ?>" title="<?=$row->galeri_title ?>">
						<!-- <img style="width: 100%;" src="<?//=streetview_image_url($row->galeri_link) ?>" alt=""> -->
						<img src="uploads/galeri/<?=$row->galeri_slug ?>/<?=$row->galeri_picture ?>" alt="">
						<div class="overlay">
							<span class="zoom">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>

	</div>
</div>