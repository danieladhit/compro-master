<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function index()
	{
		$this->load->model('user_model', 'users');
		$this->mViewData['count'] = array(
			'users' => $this->users->count_all(),
			'admission' => $this->global->count_data('admission'),
			'poster_view' => $this->global->count_data('blog_info_by_user', array(
				'page' => 'upcoming-event'
				)),
			);
		$this->render('home');
	}
}
