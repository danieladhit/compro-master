 <body class="page-index has-hero">
   <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->

   <!-- ======== @Region: #navigation ======== -->
   <div id="navigation" class="wrapper">
    <!--Header & navbar-branding region-->
    <div class="header block block-bg-noise">
      <div class="header-inner container-fluid">
        <div class="row">
          <div class="col-md-8">
            <!--navbar-branding/logo - hidden image tag & site name so things like Facebook to pick up, actual logo set via CSS for flexibility -->
            <a class="navbar-brand" href="index.html" title="Home">
              <h1 class="hidden">
                <img src="src/images/logo.png" alt="GPS Logo">
                Flexor
              </h1>
            </a>
          </div>
          <!--header rightside-->
          <div class="col-md-4">
            <!--user menu-->
            <ul class="navbar-text social-media social-media-inline pull-right list-inline">
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="">
      <div class="navbar navbar-default">
        <!--mobile collapse menu button-->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

        <!--everything within this div is collapsed on mobile-->
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav" id="main-menu">
            <li class="icon-link">
              <a href="index.html"><i class="fa fa-home"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT GPS<b class="caret"></b></a>
              <!-- Dropdown Menu -->
              <ul class="dropdown-menu">
                <li class="dropdown-header">All About GPS</li>
                <li><a href="#" tabindex="-1" class="menu-item">Achievements</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">Credo GPS</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">GPS Philosophy</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">History</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">Mars Global Prestasi</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">Vission Mission</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">INTERNATIONAL PROGRAM<b class="caret"></b></a>
              <!-- Dropdown Menu -->
              <ul class="dropdown-menu">
                <li class="dropdown-header">International Program</li>
                <li><a href="#" tabindex="-1" class="menu-item">IP Curriculum</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">IP Students Talent Performance</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">KINDERGARTEN<b class="caret"></b></a>
              <!-- Dropdown Menu -->
              <ul class="dropdown-menu">
                <li class="dropdown-header">Kindergarten</li>
                <li><a href="#" tabindex="-1" class="menu-item">Global Prestasi Montessori</a></li>
                <li><a href="#" tabindex="-1" class="menu-item">Global Prestasi Kids</a></li>
              </ul>
            </li>
            <li><a href="#">CONTACT US</a></li>
            <li class="dropdown dropdown-mm">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Other<b class="caret"></b></a>
              <!-- Dropdown Menu -->
              <ul class="dropdown-menu dropdown-menu-mm dropdown-menu-persist">
                <li class="row">
                  <ul class="col-md-12">
                    <li class="dropdown-header">ADMISSION</li>
                    <li><a href="#">Registration Info</a></li>
                  </ul>
                  <ul class="col-md-12">
                    <li class="dropdown-header">ALUMNI</li>
                    <li><a href="#">Alumni Profile</a></li>
                  </ul>
                  <ul class="col-md-12">
                    <li class="dropdown-header">VACANCY</li>
                    <li><a href="#">Recruitment Page</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!--/.navbar-collapse -->
      </div>
    </div>
  </div>