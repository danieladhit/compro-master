 <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.1">
 	<div class="hero" id="highlighted">
 		<div class="inner">
 			<!--Slideshow-->
 			<div id="highlighted-slider" class="">
 				<div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp", "autoHeight":true, "items":1, "margin":10}'>
 					<!--Slideshow content-->
 					<!--Slide 1-->
 					<?php for($i = 1; $i<=4; $i++){ ?>
 					<div class="item">
 						<img src="../src/images/slide-image-<?=$i ?>.jpg" class="img-responsive" alt="slide image" style="width: 100%;height: auto">
 					</div>
 					<?php } ?>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>


 <!-- ======== @Region: #content ======== -->
 <div id="content">
 	<!-- Mission Statement -->
 	<div class="mission text-center block block-pd-sm block-bg-primary">
 		<div class="container">
 			<h2 class="text-shadow-white">
 				To be a learning community which has a faith, global minded, high achievement and useful for families and our nation.
 				<a href="about.html" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
 			</h2>
 		</div>
 	</div>

 </div>
 <!-- /content -->

 <div class="container-fluid">
 	<div class="row">
 		<div class="col-md-8 col-sm-8">
 			<!-- Main Post -->
 			<div class="row">
 				<div class="col-md-12">
 					<div class="row">
 						<div class="col-md-8 col-sm-8">
 							<h4><strong><a href="#">Title of the post</a></strong></h4>
 						</div>
 					</div>
 					<div class="row">
 						<div class="col-md-12">
 							<a href="#" class="thumbnail">
 								<img class="img-responsive" src="../src/images/blog-image-1.jpeg" alt="">
 							</a>
 						</div>
 						<div class="col-md-12">      
 							<p>
 								Lorem ipsum dolor sit amet, id nec conceptam conclusionemque. Et eam tation option. Utinam salutatus ex eum. Ne mea dicit tibique facilisi, ea mei omittam explicari conclusionemque, ad nobis propriae quaerendum sea.
 							</p>
 							<p><a class="btn btn-sm btn-primary" href="#">Read more</a></p>
 						</div>
 					</div>
 					<div class="row">
 						<div class="col-md-8 col-sm-8">
 							<p></p>
 							<p>
 								<i class="icon-user"></i> by <a href="#">John</a> 
 								| <i class="icon-calendar"></i> Sept 16th, 2012
 								| <i class="icon-comment"></i> <a href="#">3 Comments</a>
 								| <i class="icon-share"></i> <a href="#">39 Shares</a>
 								| <i class="icon-tags"></i> Tags : <a href="#"><span class="label label-info">Snipp</span></a> 
 								<a href="#"><span class="label label-info">Bootstrap</span></a> 
 								<a href="#"><span class="label label-info">UI</span></a> 
 								<a href="#"><span class="label label-info">growth</span></a>
 							</p>
 						</div>
 					</div>
 				</div>
 			</div>
 			<hr>
 			<!-- /Main Post -->

 			<div class="row">
 				<div class="col-md-12">
 					<div class="row">
 						<div class="col-md-8 col-sm-8">
 							<h4><strong><a href="#">Title of the post</a></strong></h4>
 						</div>
 					</div>
 					<div class="row">
 						<div class="col-md-2 col-sm-2">
 							<a href="#" class="thumbnail">
 								<img src="http://placehold.it/260x180" alt="">
 							</a>
 						</div>
 						<div class="col-md-10 col-sm-10">      
 							<p>
 								Lorem ipsum dolor sit amet, id nec conceptam conclusionemque. Et eam tation option. Utinam salutatus ex eum. Ne mea dicit tibique facilisi, ea mei omittam explicari conclusionemque, ad nobis propriae quaerendum sea.
 							</p>
 							<p><a class="btn btn-sm btn-primary" href="#">Read more</a></p>
 						</div>
 					</div>
 					<div class="row">
 						<div class="col-md-8 col-sm-8">
 							<p></p>
 							<p>
 								<i class="icon-user"></i> by <a href="#">John</a> 
 								| <i class="icon-calendar"></i> Sept 16th, 2012
 								| <i class="icon-comment"></i> <a href="#">3 Comments</a>
 								| <i class="icon-share"></i> <a href="#">39 Shares</a>
 								| <i class="icon-tags"></i> Tags : <a href="#"><span class="label label-info">Snipp</span></a> 
 								<a href="#"><span class="label label-info">Bootstrap</span></a> 
 								<a href="#"><span class="label label-info">UI</span></a> 
 								<a href="#"><span class="label label-info">growth</span></a>
 							</p>
 						</div>
 					</div>
 				</div>
 			</div>
 			<hr>
 		</div>
 		
 		<!-- Sidebar -->
 		<div class="col-md-4 col-sm-4">
 			
 		</div>
 	</div>
 </div>

 <!-- Call out block -->
 <div class="block block-pd-sm block-bg-noise">
 	<div class="container">
 		<div class="row">
      <!-- <h3 class="col-md-4">
        text
    </h3> -->
    <div class="col-md-12">
    	<div class="row">
    		<!--Client logos should be within a 120px wide by 60px height image canvas-->
    		<div class="col-xs-6 col-md-2 col-sm-2">
    			<a href="" title="Client 1">
    				<img style="width: 100%" src="../src/images/diknas.png" alt="Client 1 logo" class="img-responsive">
    			</a>
    		</div>
    		<div class="col-xs-6 col-md-2 col-sm-2">
    			<a href="" title="Client 2">
    				<img style="width: 100%" src="../src/images/GEGBekasi.png" alt="Client 2 logo" class="img-responsive">
    			</a>
    		</div>
    		<div class="col-xs-6 col-md-4 col-sm-4">
    			<a href="" title="Client 3">
    				<img style="width: 100%" src="../src/images/logo_cambridge.jpg" alt="Client 3 logo" class="img-responsive">
    			</a>
    		</div>
    		<div class="col-xs-6 col-md-4 col-sm-4">
    			<a href="" title="Client 3">
    				<img style="width: 100%" src="../src/images/logo_cambridge2.png" alt="Client 3 logo" class="img-responsive">
    			</a>
    		</div>
    	</div>
    </div>
</div>
</div>
</div>