<div class="custom_content clearfix">
	<div class="container">
		<div class="photo_gallery custom">
			<ul class="gallery popup-gallery gallery-4col">
				<?php foreach($images as $key => $row){ ?>
				<li>
					<a href="uploads/galeri/<?=$row->galeri_slug.'/'.$row->galeri_picture ?>" title="<?=$row->galeri_title ?>">
						<img style="width: 100%;" src="<?= image('uploads/galeri/'.$row->galeri_slug.'/'.$row->galeri_picture, 'gallery') ?>" alt="">
						<div class="overlay">
							<span class="zoom">
								<i class="fa fa-search"></i>
							</span>
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>

	</div>
</div>