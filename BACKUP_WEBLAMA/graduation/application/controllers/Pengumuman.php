<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengumuman extends CI_Controller {
   var $title = 'Halaman administrasi siswa';
   var $limit = 5;
 
	function _construct()
	{
		parent::_construct();
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url'));
		$this->load->model('pengumuman_model','',true);
	}
 
    public function index()
    {
    $this->load->helper(array('form', 'url'));	 
        //$data['main_view'] = 'cari_nilai';khusus sma
        $data['main_view'] = 'cari_nilai';
  	$data['form_action']	= site_url('pengumuman/proses_cari');
       $this->load->view('template_kelulusan',$data);
	}
    function proses_cari()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('pengumuman_model','',true);
		 $this->load->helper('text');
	     $data['title']=$this->title;        
  		$user=$this->input->post('judul');
  		$pass=$this->input->post('password');	
  	    $data['cari']= $this->pengumuman_model->getjudul($user,$pass);	
		$query= $this->pengumuman_model->count_cari($user,$pass);	
		$data['total_cari']= $query->num_rows();
		//$data['main_view'] = 'result_unsma';khusus sma
		$data['main_view'] = 'result_unsma';
	    $this->load->view('template_kelulusan',$data);
		
	}
  function code($id)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('artikel_model','',true);
		//id_berita,id_kategori,username,judul,headline,berita,hari,tanggal,waktu
		$user = $this->artikel_model->getKd($id);
		$data['judul']= $user->judul;
		$data['berita']=$user->berita;
		$data['hari']=$user->hari;
		$data['tanggal']=$user->tanggal;
		$data['waktu']=$user->waktu;		
		$data['main_view'] = 'detail-news';
		$this->load->view('template', $data);
	}  
	
	function facility()
    {
      $data['main_view'] = 'view-facility';
  	  $this->load->model('gallery_model', '', TRUE);
     	$data['base_url'] = site_url('home_user/facility');
  		$data['total_rows'] = $this->db->count_all('gallery');
  		$uri_segment = 3;
  		$offset = $this->uri->segment($uri_segment);
  		$this->pagination->initialize($data);
  	 	$data['gambar'] =$this->gallery_model->gallery();
       $this->load->view('template',$data);
    }

	function simpan()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('page_model','',true);
		$this->load->model('kelas_model','',true);
		$data['form_action']	= site_url('siswa/simpan');
		/* bagian validasi*/
		$listpro=$this->page_model->getpro();	
		$data['provinsi']=$listpro;
		$this->form_validation->set_rules('Tanggal_Daftar', 'Tanggal_Daftar', 'required');
   		$this->form_validation->set_rules('Nama_Diklat', 'Nama_Diklat', 'required');
		$this->form_validation->set_rules('Nama_Siswa', 'Nama_Siswa', 'required');
		$this->form_validation->set_rules('NIP', 'NIP', 'required');
		$this->form_validation->set_rules('Pangkat', 'Pangkat', 'required');
		$this->form_validation->set_rules('Golongan', 'Golongan', 'required');
		$this->form_validation->set_rules('Jenis_Kelamin', 'Jenis_Kelamin', 'required');
		$this->form_validation->set_rules('Tempat_Lahir', 'Tempat_Lahir', 'required');
		$this->form_validation->set_rules('Tanggal_Lahir', 'Tanggal_Lahir', 'required');
		$this->form_validation->set_rules('No_Telp', 'No_Telp', 'required');
		$this->form_validation->set_rules('Email_Pribadi', 'Email_Pribadi', 'required');
		$this->form_validation->set_rules('Nama_Instansi', 'Nama_Instansi', 'required');
		$this->form_validation->set_rules('Nama_Atasan', 'Nama_Atasan', 'required');
		$this->form_validation->set_rules('Alamat_Instansi', 'Alamat_Instansi', 'required');
		//waktu input
		 $breadcrumb = array( 
      				"Home" => "..", 
      				"Tambah Siswa" => ""     
      				); 
          $data['breadcrumb'] = $breadcrumb; 
		$wk=date('Y-m-d H:i:s');
		$year=date('Y');
		$prov=$this->input->post('provinsi');
		$tot=$this->kelas_model->count_prov($prov);
		$tot_kuota=$this->kelas_model->count_kuota($prov);
		if ($this->form_validation->run() == TRUE)
		{ foreach($tot->result() as $row){
			if(($row->total) > ($tot_kuota->kuota))
				{
				$data['main_view'] = 'pesan_kunci';
       		    $this->load->view('template_user', $data);
				}
				else{
			  if ($this->page_model->getkuncipro($prov))
			   {
				
				
				//No_Pendaftaran Kelas Tahun_Ajaran
			   
			    $kelas = array(
					'NIP'=> $this->input->post('NIP'),
					'Tahun_Anggaran'=> $year
					);
			$this->page_model->addkelas($kelas);						
			$news = array(
					'Tanggal_Pendaftaran'=> $this->input->post('Tanggal_Daftar'),
					'Nama_Diklat'=> $this->input->post('Nama_Diklat'),
					'Nama_Siswa'=> $this->input->post('Nama_Siswa'),
					'NIP'=> $this->input->post('NIP'),
					'Pangkat'=> $this->input->post('Pangkat'),
					'Golongan'=> $this->input->post('Golongan'),
					'provinsi'=> $this->input->post('provinsi'),
					'Jenis_Kelamin'=> $this->input->post('Jenis_Kelamin'),
					'Tempat_Lahir'=> $this->input->post('Tempat_Lahir'),
					'Tanggal_Lahir'=> $this->input->post('Tanggal_Lahir'),
					'No_Telp'=> $this->input->post('No_Telp'),
					'Email_Pribadi'=> $this->input->post('Email_Pribadi'),
					'Nama_Instansi'=> $this->input->post('Nama_Instansi'),
					'Nama_Atasan'=> $this->input->post('Nama_Atasan'),
					'Alamat_Instansi'=> $this->input->post('Alamat_Instansi')
					);
		    $this->page_model->add($news);
			$data['main_view'] = 'pesan';
       		$this->load->view('template_user', $data);
			
			}else{
			$news = array(
					'Tanggal_Pendaftaran'=> $this->input->post('Tanggal_Daftar'),
					'Nama_Diklat'=> $this->input->post('Nama_Diklat'),
					'Nama_Siswa'=> $this->input->post('Nama_Siswa'),
					'NIP'=> $this->input->post('NIP'),
					'Pangkat'=> $this->input->post('Pangkat'),
					'Golongan'=> $this->input->post('Golongan'),
					'provinsi'=> $this->input->post('provinsi'),
					'Jenis_Kelamin'=> $this->input->post('Jenis_Kelamin'),
					'Tempat_Lahir'=> $this->input->post('Tempat_Lahir'),
					'Tanggal_Lahir'=> $this->input->post('Tanggal_Lahir'),
					'No_Telp'=> $this->input->post('No_Telp'),
					'Email_Pribadi'=> $this->input->post('Email_Pribadi'),
					'Nama_Instansi'=> $this->input->post('Nama_Instansi'),
					'Nama_Atasan'=> $this->input->post('Nama_Atasan'),
					'Alamat_Instansi'=> $this->input->post('Alamat_Instansi')
					);	
			  $this->page_model->add_gagal($news);		
			$data['main_view'] = 'pesan';
            $this->load->view('template_user', $data);
			}
				}
		}
		}
		else
		{
		$data['main_view'] = 'siswa_form';
		$this->load->view('template_user', $data);
		}	
	}
	
}