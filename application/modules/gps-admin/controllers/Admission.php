<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('admission');
		$crud->columns('name','email','phone','grade','status','created_at');
		// disable direct create / delete Frontend User
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();

		$this->mPageTitle = 'Admission';
		$this->render_crud();
	}

	// Frontend User CRUD
	public function poster_view()
	{
		$crud = $this->generate_crud('blog_info_by_user');
		$crud->unset_columns('mac_address','blog_id');
		$crud->where('page','upcoming-event');

		// disable direct create / delete Frontend User
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();

		$this->mPageTitle = 'Poster View';
		$this->render_crud();
	}
}
