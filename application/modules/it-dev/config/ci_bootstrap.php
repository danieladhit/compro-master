<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'IT Dev',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),
	
	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			'assets/dist/admin/adminlte.min.js',
			'assets/dist/admin/lib.min.js',
			'assets/dist/admin/app.min.js'
		),
		'foot'	=> array(
			'src/plugins/jquery-ui/jquery-ui.min.js',
			'src/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
			'src/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js',
			'src/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
			'src/plugins/moment/moment.js',
			'src/plugins/moment/moment-range.js',
			'src/plugins/calendar/fullcalendar.min.js',
			'src/plugins/select2/select2.full.min.js',
			'src/js/schedule_config.js',
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'src/plugins/select2/select2.min.css',
			'assets/dist/admin/adminlte.min.css',
			'assets/dist/admin/lib.min.css',
			'assets/dist/admin/app.min.css',
			'src/plugins/calendar/fullcalendar.min.css',
			'src/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
			'src/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
		)
	),

	// Default CSS class for <body> tag
	'body_class' => '',
	
	// Multilingual settings
	'languages' => array(
	),

	// Menu items
	'menu' => array(
		'dashboard' => array(
			'name'		=> 'Dashboard',
			'url'		=> 'dashboard',
			'icon'		=> 'fa fa-dashboard',
		),
		'rooms' => array(
			'name'		=> 'Manage Rooms',
			'url'		=> 'rooms',
			'icon'		=> 'fa fa-home',
		),
		'equipment' => array(
			'name'		=> 'Manage Equipment',
			'url'		=> 'equipment',
			'icon'		=> 'fa fa-tasks',
		),
		'schedule' => array(
			'name'		=> 'Meeting Calendar',
			'url'		=> '',
			'icon'		=> 'fa fa-calendar',
		),
		'panel' => array(
			'name'		=> 'Admin Panel',
			'url'		=> 'panel',
			'icon'		=> 'fa fa-cog',
			'children'  => array(
				'Admin Users'			=> 'panel/admin_user',
				'Create Admin User'		=> 'panel/admin_user_create',
				'Admin User Groups'		=> 'panel/admin_user_group',
			)
		),
		'logout' => array(
			'name'		=> 'Sign Out',
			'url'		=> 'panel/logout',
			'icon'		=> 'fa fa-sign-out',
		)
	),

	// Login page
	'login_url' => 'it-dev/login',

	// Restricted pages
	'page_auth' => array(
		'dashboard'					=> array('webmaster','superadmin', 'admin'),
		'user/create'				=> array('webmaster'),
		'user/group'				=> array('webmaster'),
		'panel'						=> array('webmaster'),
		'panel/admin_user'			=> array('webmaster'),
		'panel/admin_user_create'	=> array('webmaster'),
		'panel/admin_user_group'	=> array('webmaster'),
		'util'						=> array('webmaster'),
		'util/list_db'				=> array('webmaster'),
		'util/backup_db'			=> array('webmaster'),
		'util/restore_db'			=> array('webmaster'),
		'util/remove_db'			=> array('webmaster'),
		'schedule'					=> array('webmaster', 'superadmin', 'admin'),
		'rooms'						=> array('webmaster', 'superadmin'),
		'equipment'					=> array('webmaster', 'superadmin'),
	),

	// AdminLTE settings
	'adminlte' => array(
		'body_class' => array(
			'webmaster'	=> 'skin-red',
			'superadmin'	=> 'skin-black',
			'admin'		=> 'skin-purple',
		)
	),

	// Useful links to display at bottom of sidemenu
	'useful_links' => array(
		
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_it-dev';