<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipment extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('calendar_equipment');
		$crud->columns('name', 'description');
		$crud->fields('name', 'description');

		$this->mPageTitle = 'Manage Equipment';
		$this->render_crud();
	}

}
