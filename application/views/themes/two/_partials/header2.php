<header class="header-wrapper header-v3" style="height: auto;">
  <div class="topbar clearfix">
    <div class="container">
      <a style="margin-top: 20px;" class="navbar-brand logo clearfix" href="<?=base_url() ?>"><img style="height: auto; width: 100%;" src="src/images/logo-gps.png" alt="Logo" class="img-responsive"></a>
     <!--  <div class="school-name" style="float: left;margin-top: 10px;margin-left: 10px;">
         <h3 class="cr-blue h3" style="margin-bottom: 0px;font-family: 'Open Sans', sans-serif;">GLOBAL PRESTASI SCHOOL</h3>
         <h4 style="font-family: 'Open Sans', sans-serif; margin-top: 0px;" class="cr-red h4">Empowering Global Education</h4>
     </div> -->
      <div class="topbar-right">
        <div class="topContact clearfix" style="width: 43%">
          <div class="phone">
            <div class="top-icon">
              <a href="tel: +62 21 88852668"><i class="fa fa-phone"></i></a>
            </div>
            <div class="callus" style="padding-left: 5px;">
              <h5>Call Us</h5>
              <span>+62 21 88852668</span>
            </div>
          </div>
        </div>
        <div class="topContact clearfix"  style="width: 57%">
          <div class="mail">
            <div class="top-icon">
              <a href="mailto:info@yourdomain.com"><i class="fa fa-envelope"></i></a>
            </div>
            <div class="mailto" style="padding-left: 5px;">
              <h5>Email</h5>
              <span>info@globalprestasi.sch.id</span>
            </div>
          </div>
        </div>
            <!-- <div class="input-group">
              <input type="text" class="form-control" placeholder="Search…" aria-describedby="basic-addon2">
              <button type="submit" class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div> -->
          </div>
        </div>
      </div>

      
      <div class="header clearfix" style="opacity: 1;">
        <nav class="navbar navbar-main navbar-default">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="header_inner">

                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="main-nav" style="margin-top: 0px;">
                    <ul class="nav navbar-nav navbar-right">
                      <li class="<?=$this->nav == 'home' ? 'active' : '' ?>">
                        <a href="<?=base_url() ?>" class="dropdown-toggle" role="button" aria-expanded="false">Home</a>
                      </li>
                      <li class="dropdown <?=$this->nav == 'about-gps' ? 'active' : '' ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ABOUT US</a>
                        <!-- Dropdown Menu -->
                        <ul class="dropdown-menu">
                          <li class="dropdown-header">All About GPS</li>
                          <li class="<?=$this->nav_child == 'history' ? 'active' : '' ?>"><a href="<?=base_url('about-gps/history') ?>">History, Vision Mission, Philosophy</a></li>
                          <li class="<?=$this->nav_child == 'excelence-program' ? 'active' : '' ?>"><a href="<?=base_url('about-gps/excelence-program') ?>">Excelence Program</a></li>
                          <li class="<?=$this->nav_child == 'facilities' ? 'active' : '' ?>"><a href="<?=base_url('about-gps/facilities') ?>">Facilities</a></li>
                          <li class="<?=$this->nav_child == 'mars-gps' ? 'active' : '' ?>"><a href="<?=base_url('about-gps/mars-gps') ?>">Mars Global Prestasi</a></li>
                        </ul>
                      </li>
                      <li class="dropdown <?=$this->nav == 'our-school' ? 'active' : '' ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">OUR SCHOOL</a>
                        <!-- Dropdown Menu -->
                        <ul class="dropdown-menu">
                          <li class="<?=$this->nav_child == 'gp-montessori' ? 'active' : '' ?>"><a href="<?=base_url('our-school/gp-montessori') ?>">GP Montessori</a></li>
                          <li class="<?=$this->nav_child == 'gp-elementary' ? 'active' : '' ?>"><a href="<?=base_url('our-school/gp-elementary') ?>">GP Elementary</a></li>
                          <li class="<?=$this->nav_child == 'gp-jhs' ? 'active' : '' ?>"><a href="<?=base_url('our-school/gp-jhs') ?>">GP Junior High School</a></li>
                          <li class="<?=$this->nav_child == 'gp-shs' ? 'active' : '' ?>"><a href="<?=base_url('our-school/gp-shs') ?>">GP Senior High School</a></li>
                          <li class="<?=$this->nav_child == 'gp-kids' ? 'active' : '' ?>"><a href="<?=base_url('our-school/gp-kids') ?>">GP Kids</a></li>
                          <li class="<?=$this->nav_child == 'harapan_mulia' ? 'active' : '' ?>"><a href="<?=base_url('our-school/harapan-mulia') ?>">Harapan Mulia</a></li>
                        </ul>
                      </li>
                      <li class="dropdown <?=$this->nav == 'gallery' || $this->nav == 'videos' || $this->nav == 'images'  ? 'active' : '' ?>">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
                        <!-- Dropdown Menu -->
                        <ul class="dropdown-menu">
                        <li class="dropdown-header">Gallery</li>
                          <!-- <li class="<?//=$this->nav_child == 'achievements' ? 'active' : '' ?>"><a href="<?//=base_url('gallery/achievements') ?>">Achievements</a></li>
                          <li class="<?//=$this->nav_child == 'events' ? 'active' : '' ?>"><a href="<?//=base_url('gallery/events') ?>">Events</a></li>
                          <li class="<?//=$this->nav_child == 'school_activity' ? 'active' : '' ?>"><a href="<?//=base_url('gallery/school_activity') ?>">School Activity</a></li> -->
                          <!-- <li class="<?//=$this->nav == 'achievement' ? 'active' : '' ?>"><a href="<?//=base_url('achievement') ?>">Achievement</a></li> -->
                          <li class="<?=$this->nav == 'images' ? 'active' : '' ?>"><a href="<?=base_url('images') ?>">Facility</a></li>
                          <li class="<?=$this->nav == 'videos' ? 'active' : '' ?>"><a href="<?=base_url('videos') ?>">Videos</a></li>
                        </ul>
                      </li>
                      <li class="<?=$this->nav == 'admission' ? 'active' : '' ?>"><a href="<?=base_url('admission') ?>">ADMISSION</a></li>
                      <li class="<?=$this->nav == 'alumni' ? 'active' : '' ?>"><a href="<?=base_url('alumni') ?>">ALUMNI</a></li>
                      <li class="<?=$this->nav == 'contact-us' ? 'active' : '' ?>"><a href="<?=base_url('contact-us') ?>">CONTACT US</a></li>
                      <li class="<?=$this->nav == 'vacancy' ? 'active' : '' ?>"><a href="<?=base_url('vacancy') ?>">VACANCY</a></li>
                    </ul>
                  </div><!-- navbar-collapse -->


                </div>
              </div>
            </div>
          </div><!-- /.container -->
        </nav><!-- navbar -->
      </div></header>