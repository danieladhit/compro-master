<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Global Prestasi School',

	// google api key
	'google_api_key' => 'AIzaSyAshH-WsVH1fx2b0nNdnqH-iSpWu66N9N4',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
		),
		'foot'	=> array(
			'http://themes.iamabdus.com/royal/1.3/plugins/jquery/jquery-1.11.1.min.js',
			'src/themes/one/lib/bootstrap/js/bootstrap.min.js',
			'http://themes.iamabdus.com/royal/1.3/plugins/flexslider/jquery.flexslider.js',
			'http://themes.iamabdus.com/royal/1.3/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js',
			'http://themes.iamabdus.com/royal/1.3/plugins/selectbox/jquery.selectbox-0.1.3.min.js',
			'http://themes.iamabdus.com/royal/1.3/plugins/animation/wow.min.js',
			'src/themes/one/lib/owlcarousel/owl.carousel.min.js',
			'src/themes/one/lib/stellar/stellar.min.js',
			'src/themes/one/lib/waypoints/waypoints.min.js',
			'src/themes/one/lib/counterup/counterup.min.js',
			'src/themes/one/contactform/contactform.js',
			/*'src/themes/one/js/custom.js',*/
			'src/js/royal.js',
			'src/themes/one/js/color-switcher.js',
			'src/plugins/pop-up/jquery.magnific-popup.js',
			'http://themes.iamabdus.com/royal/1.3/plugins/timer/jquery.syotimer.js'
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900',
			'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700',
			'src/themes/one/lib/bootstrap/css/bootstrap.min.css',
			'src/themes/one/lib/font-awesome/css/font-awesome.min.css',
			'src/themes/one/lib/owlcarousel/owl.carousel.min.css',
			'src/themes/one/lib/owlcarousel/owl.theme.min.css',
			'src/themes/one/lib/owlcarousel/owl.transitions.min.css',
			/*'src/themes/one/css/style.css',
			'src/themes/one/css/colour-blue.css',*/
			'src/css/frontend.css',
			'src/plugins/pop-up/magnific-popup.css'
		)
	),

	// Default CSS class for <body> tag
	'body_class' => 'body-wrapper version3',
	
	// Multilingual settings
	'languages' => array(
		'default'		=> 'en',
		'autoload'		=> array('general'),
		'available'		=> array(
			'en' => array(
				'label'	=> 'English',
				'value'	=> 'english'
			),
			'zh' => array(
				'label'	=> '繁體中文',
				'value'	=> 'traditional-chinese'
			),
			'cn' => array(
				'label'	=> '简体中文',
				'value'	=> 'simplified-chinese'
			),
			'es' => array(
				'label'	=> 'Español',
				'value' => 'spanish'
			)
		)
	),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => 'tes',

	// Restricted pages
	'page_auth' => array(
	),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',
		
		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_frontend';