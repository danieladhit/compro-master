 <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.1">
 	<div class="hero" id="highlighted">
 		<div class="inner">
 			<!--Slideshow-->
 			<div id="highlighted-slider" class="">
 				<div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp", "autoHeight":true, "items":1, "margin":10}'>
 					<!--Slideshow content-->
 					<!--Slide 1-->
 					<?php for($i = 1; $i<=4; $i++){ ?>
 					<div class="item">
 						<img src="../src/images/slide-image-<?=$i ?>.jpg" class="img-responsive" alt="slide image" style="width: 100%;height: auto">
 					</div>
 					<?php } ?>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>


 <div class="container">

  <!--NEWS SECTION -->
  <div class="news-section padding">
    <div class="row">
      <div class="col-sm-8 col-xs-12">
        <div class="related_post_sec single_post news-content">
          <div class="sectionTitle title-block">
            <h3>What’s happening in GPS?</h3>
          </div>
          <a class="more" href="">View All &gt;</a>
          <div class="row news-box">
            <?php foreach($event as $key => $row){ ?>
            <div class="col-sm-6 col-xs-12">
              <div class="news-boxInner">
                <span class="rel_thumb">
                  <a href="single-post-right-sidebar.html"><img src="<?=upload_url('blog/'.$row->blog_picture) ?>" alt="Image"></a>
                </span><!--end rel_thumb-->
                <div class="rel_right">
                  <h4><a href="single-post-right-sidebar.html"><?=$row->blog_title ?></a></h4>
                  <div class="meta">
                    <span class="author">Posted in: <a href="#">Update</a></span>
                    <span class="date">on: <a href="#">January 24, 2015</a></span>
                  </div>
                </div><!--end rel right-->
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>

      <div class="col-sm-4 col-xs-12">
        <div class="upcoming_events event-col">
          <div class="related_post_sec single_post">
            <div class="sectionTitle title-block">
              <h3>Upcoming event</h3>
            </div>
            <a class="more" href="">View All &gt;</a>
            <div class="rel_right">
              <h4><a href="single-events.html"><?=$upcoming_event->blog_title ?></a></h4>
              <div class="meta">
                <span class="place"><i class="fa fa-map-marker"></i>Main Campus</span>
                <span class="event-time"><i class="fa fa-clock-o"></i>11.00 pm</span>
                <span class="publish-date">Start Date Is : <a href="#">January 24, 2017</a></span>
              </div>
              <div id="event_timer" class="timer"><div class="timer-head-block"></div><div class="timer-body-block">            <div class="table-cell day">                <div class="tab-val">244</div>                <div class="tab-metr">days</div>            </div>            <div class="table-cell hour">                <div class="tab-val">10</div>                <div class="tab-metr">hours</div>            </div>            <div class="table-cell minute">                <div class="tab-val">20</div>                <div class="tab-metr">mins</div>            </div>            <div class="table-cell second">                <div class="tab-val" style="opacity: 1;">01</div>                <div class="tab-metr">sec</div>            </div></div><div class="timer-foot-block"></div></div>
              <a href="single-events.html" class="btn btn-default commonBtn">Join now</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- LIGHT SECTION -->
  <div class="clearfix padding feature-section1">
    <div class="sectionTitle text-center">
      <h3>Core Values</h3>
    </div>

    <div class="row">
      <div class="col-sm-2 col-sm-offset-1 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-heart" aria-hidden="true"></i></span>
          <h3>Care</h3>

        </div>
      </div>

      <div class="col-sm-2 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i></span>
          <h3>Competency</h3>
        </div>
      </div>

      <div class="col-sm-2 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
          <h3>Commitment</h3>
        </div>
      </div>

      <div class="col-sm-2 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-gears" aria-hidden="true"></i></span>
          <h3>Core Teamwork</h3>
        </div>
      </div>

      <div class="col-sm-2 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-key" aria-hidden="true"></i></span>
          <h3>Consistency</h3>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- paralax 1 -->
<div class="padding clearfix paralax" style="background-image: url(../src/images/paralax01.jpg);">
  <div class="container">
    <div class="paralax-text text-center paralaxInner">
      <h2>Join 2591 Students</h2>
      <p>Donec congue consequat risus, nec volutpat enim tempus id. Proin et sapien eget diam ullamcorper consectetur. Sed blandit imperdiet mauris. Mauris eleifend faucibus ipsum quis varius. Quisque pharetra leo erat, non eleifend nibh interdum quis.</p>
      <a href="buying-steps.html" class="btn btn-default commonBtn">Get Admission</a>
    </div><!-- row -->
  </div>
</div>

<!-- VIDEO SECTION -->
<div class="video-section padding">
  <div class="container">
    <div class="sectionTitle title-block">
      <h3>What’s happening in GPS?</h3>
    </div>
    <a class="more" href="">View All &gt;</a>
    <div class="row">
      <div class="col-md-6 col-xs-12">
        <div class="videoLeft">
          <div class="related_post_sec single_post">
            <span class="rel_thumb">
              <a class="popup-youtube" href="<?=$first_video->video_link ?>" >
                <img src="http://img.youtube.com/vi/<?=youtube_id($first_video->video_link) ?>/hqdefault.jpg"></a>
                <div class="rel_right">
                  <h4><a href="single-post-right-sidebar.html"><?=$first_video->video_title ?></a></h4>
                  <div class="meta">
                    <span class="author">Posted by: <a href="#">Admin </a></span>
                    <span class="date">on: <a href="#">January 24, 2016</a></span>
                  </div>
                </div>
              </span></div>
            </div>
          </div>

          <div class="col-md-6 col-xs-12">
            <div class="videoRight">
              <div class="related_post_sec single_post">
                <ul>
                  <?php foreach($video as $key => $row){ ?>
                  <li>
                    <span class="rel_thumb">
                      <a class="popup-youtube" href="<?=$row->video_link ?>" savefrom_lm_index="2">
                        <img src="http://img.youtube.com/vi/<?=youtube_id($row->video_link) ?>/hqdefault.jpg">
                      </a>
                    </span>
                    <div class="rel_right">
                      <h4><a href="single-post-right-sidebar.html"><?=$row->video_title ?></a></h4>
                      <div class="meta">
                        <span class="author">Posted by: <a href="#">Admin </a></span>
                        <span class="date">on: <a href="#">January 24, 2016</a></span>
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--  paralax 2 (connect with us) -->
    <div class="padding clearfix paralax" style="background-image: url(../src/images/paralax02.jpg);">
      <div class="container">
        <div class="paralax-text text-center paralaxInner">
          <h2>Connect with us</h2>
          <p>Simply Follow, Like or Connect with us</p>
          <ul class="list-inline">
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
          </ul>
        </div><!-- row -->
      </div>
    </div>

    <div class="clearfix padding gallery-section">
      <div class="container">
        <div class="sectionTitle title-block">
          <h3>Gallery</h3>
        </div>
        <a class="more" href="">View All &gt;</a>
        <div class="custom photo_gallery">
          <ul class="gallery">
            <?php foreach($galery_street_view as $key => $row){ ?>
            <li>
              <a class="popup-gmaps" href="<?=get_iframe_src($row->galeri_description) ?>" title="<?=$row->galeri_title ?>">
                <img src="<?=streetview_image_url($row->galeri_link) ?>" alt="">
                <div class="overlay">
                  <span class="zoom">
                    <i class="fa fa-search"></i>
                  </span>
                </div>
              </a>
            </li>
          <?php } ?>
          </ul>
        </div>
      </div>
    </div>

    <?php $clients = array('diknas.png', 'GEGBekasi.png', 'logo_cambridge.jpg', 'logo_cambridge2.png') ?>
    <!-- ******Logos Section****** -->
    <div class="clearfix"></div>
    <section class="padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "autoplay":true, "loop":true}'>

              <?php foreach($clients as $client) { ?>
              <div class="item col">
                <div class="covered-img">
                  <div class="vc-table">
                    <div class="vc-cell">
                      <img src="../src/images/<?php echo $client?>" />
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>  
            </div>
          </div>
        </div>              
      </div><!--//container-->
    </section><!--//logos-->


