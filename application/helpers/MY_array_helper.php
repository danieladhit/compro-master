<?php

if ( ! defined('BASEPATH') ) {
	exit("No direct script access allowed");
}

function array_hash($arrayOrObject) {
	if (is_array($arrayOrObject)) {
		$object = (object) $arrayOrObject;
	} else {
		$object = $arrayOrObject;
	}

	$hash = array();
	foreach ($object as $key => $value) {
		$prop = get_object_vars($value);
		$hash[$prop['key']] = $prop['value'];
	}

	return $hash;
}


