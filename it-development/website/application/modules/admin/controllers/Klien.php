<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Klien extends MY_Controller {

	function index()
	{
		if($this->session->userdata('login_admin') == true or $this->session->userdata('login_editor') == true or $this->session->userdata('login_content') == true)
		{
			$user_id 				= $this->session->userdata('id_user');
			$judul					= $this->model_utama->get_detail('1','setting_id','setting')->row()->website_name;
			$data['title'] 			= 'Halaman Kelola klien  | '.$judul;
			$data['heading'] 		= 'Kelola Klien ';
			$data['form_action'] 	= site_url('admin/klien/add_picture_process');
			
			$data['list_klien']		= $this->model_utama->get_data('logo_klien');
			
			$data['page']			= 'admin/klien/page_klien_picture_form';
			$this->load->view('admin/template', $data);
			$log['user_id']			= $this->session->userdata('id_user');
			$log['activity']		= 'klik ubah data klien ';
			$this->model_utama->insert_data('log_user', $log);
		}
		else
		{
			redirect('login');
		}
	}	
	
	function add_picture_process()
	{
		if($this->session->userdata('login_admin') == true or $this->session->userdata('login_editor') == true or $this->session->userdata('login_content') == true)
		{
			$config['upload_path'] 		= './uploads/klien/';
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|rar|zip';
			
			$image_folder_path 			= 'uploads/klien/thumb';
			$file_dokumen 				= $this->upload_photo( 	$image_folder_path,
																$config );

			if( $file_dokumen != '' ){
				$data = array (
								'filename' 			=> $this->security->xss_clean($file_dokumen),
								'create_date'		=> date('Y-m-d')
								);
			}
			
			$this->session->set_flashdata("success","gambar berhasil di upload");
			
			$this->model_utama->insert_data('logo_klien',$data);
		
			redirect('admin/klien');
		}
		else
		{
			redirect('login');
		}
	}
	
	function delete($kode)
	{
		if($this->session->userdata('login_admin') == true or $this->session->userdata('login_editor') == true or $this->session->userdata('login_content') == true)
		{
			$this->db->query("delete from logo_klien where logo_klien_id = '$kode'");
		
			$this->session->set_flashdata("success","gambar berhasil dihapus");
		
			redirect('admin/klien');
		}
		else
		{
			redirect('login');
		}
	}
}