$(function () {

    /* initialize the external events
    -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          eventId : $(this).data('id'),
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    function dayCheck(date) {
     var myDate = new Date();

       //How many days to add from today?
       var daysToAdd = 0;

       myDate.setDate(myDate.getDate() + daysToAdd);
       if (date < myDate) {
         alert("You cannot book on this day!");
         return false;
       } else {
         return true;
       }
     }

     init_events($('#external-events div.external-event'))

    /* initialize the calendar
    -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
    m    = date.getMonth(),
    y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      allDaySlot: false,
      timeFormat: 'H:mm' ,
      eventSources: [
      {
       events: function(start, end, timezone, callback) {
         $.ajax({
           url: 'schedule/get_events',
           dataType: 'json',
           data: {
                 // our hypothetical feed requires UNIX timestamps
                 start: start.unix(),
                 end: end.unix()
               },
               success: function(msg) {
                 var events = msg.events;
                 callback(events);
               }
             });
       }
     },
     ],


     dayClick: function(date, jsEvent, view) {
      date_last_clicked = $(this);
      var startDate = moment(date._d).format('DD MMMM YYYY');
      //$(this).css('background-color', '#bed7f3');

      var myDate = new Date();

      //How many days to add from today?
      var daysToAdd = -1;

      myDate.setDate(myDate.getDate() + daysToAdd);

      if (date < myDate) {
      //TRUE Clicked date smaller than today + daysToadd
      alert("You cannot book on this day!");
    } else {
        //FLASE Clicked date larger than today + daysToadd
        $('#addModal').find('#start').val(startDate);
        $('#addModal').find('#hour').val("+1 hour");
        $('#addModal').modal();
      }
      
    },

    eventClick: function(event, jsEvent, view) {
      var start = moment(new Date(event.start)).format();
      var end = moment(new Date(event.end)).format();
      var range = moment.range(start, end);

      rangeInHour = range.diff('hour');
      rangeInMinute = range.diff('minute')%60 == 0 ? '' : "+"+range.diff('minute')%60+" minutes";

      console.log(event);
      var currColorModal = event.backgroundColor;

      // update select2 multiple dropdown
      $('#editModal').find('#equipment').val(event.equipment);
      $('#editModal').find('#equipment').trigger('change');
      
      console.log(event.participant);
      $('#editModal').find('#participant').val(event.participant_email);
      $('#editModal').find('#participant').trigger('change');

      $('#editModal').find(".modal-header").css({ 'background-color': currColorModal, 'border-color': currColorModal });
      $('#editModal').find("#btn-submit").css({ 'background-color': currColorModal, 'border-color': currColorModal });

      $('#editModal').find('#name').val(event.title);
      $('#editModal').find('#description').val(event.description);
      $('#editModal').find('#pic').val(event.pic);

      var hour = moment(event.start).format('DD MMMM YYYY');
      var time = moment(event.start).format('H:mm');
      
      $('#editModal').find('#start').val(hour);
      $('#editModal').find('#time').val(time);

      $('#editModal').find('#hour').val("+"+rangeInHour+" hour");
      $('#editModal').find('#minute').val(rangeInMinute);
      
      $('#editModal').find('#event_id').val(event.id);
      $('#editModal').find("#backgroundColor").val(currColorModal);
      $('#editModal').find("#borderColor").val(currColorModal)


      updateHourAndTime(hour,time);

      $('#editModal').modal();

    },

    eventDrop: function(event, delta, revertFunc) {

      if ( ! dayCheck(event.start) ) {
        revertFunc();
        return;
      }

      alert(event.title + " was dropped on " + event.start.format('DD MMMM YYYY H:mm'));
      console.log(event);
      if (!confirm("Are you sure about this change?")) {
        revertFunc();
      } else {
       $.ajax({
         url: 'schedule/edit_drop_event',
         method: 'POST',
         dataType: 'json',
         data: {
                 // our hypothetical feed requires UNIX timestamps
                 eventid: event.id,
                 start: moment(event.start).format('DD MMMM YYYY H:mm'),
                 end: moment(event.end).format('DD MMMM YYYY H:mm')
               },
               success: function(response) {

                if (! response.status) {
                  window.location.reload();
                } else {
                  alert(response.message);
                }

              },
              error : function() {
               alert('Connection Failed');
             }
           });
     }

   },

   editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        console.log(originalEventObject);
        var title = originalEventObject.title;
        var currColorModal = copiedEventObject.backgroundColor;

        var startDate = moment(date._d).format('DD MMMM YYYY');

        $('#addModal').find(".modal-header").css({ 'background-color': currColorModal, 'border-color': currColorModal });
        $('#addModal').find("#btn-submit").css({ 'background-color': currColorModal, 'border-color': currColorModal });
        $('#addModal').find("#name").val(title);
        $('#addModal').find('#start').val(startDate);
        
        $('#addModal').find("#backgroundColor").val(currColorModal);
        $('#addModal').find("#borderColor").val(currColorModal)
        $('#addModal').modal();

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        //$('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          if (confirm('Are you sure for delete this event from Events Draggable List ?')) {
            var eventId = originalEventObject.eventId;
            $(this).remove();
            removeEventsDraggable(eventId);
          }
          
        }

      }
    })


    // helper function 
    function updateHourAndTime(hour,time)
    {
      //$(".datepicker").datepicker("setValue", hour);
      $(".timepicker").timepicker("setTime", time);
    }

    function removeEventsDraggable(eventId) {
      // remove events draggable from database
      $.ajax({
       url: 'schedule/remove_draggable_event',
       method: 'POST',
       dataType: 'json',
       data: {
                 // our hypothetical feed requires UNIX timestamps
                 event_drag_id: eventId,
               },
               success: function(response) {

                if (response.status) {
                  //window.location.reload();
                } else {
                  alert('Remove draggable event Fail !!');
                }

              },
              error : function() {
               alert('Connection Failed');
             }
           });
    }

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default

    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
    })

    $('#add-new-event').click(function (e) {

      e.preventDefault()

        //Get value and make sure it is not null
        var val = $('#new-event').val()
        if (val.length == 0) {
          return
        }

        $.ajax({
         url: 'schedule/add_draggable_event',
         method: 'POST',
         dataType: 'json',
         data: {
                 // our hypothetical feed requires UNIX timestamps
                 label: val,
                 backgroundColor: currColor,
                 color: '#fff',
               },
               success: function(response) {

                if (response.status) {
                  //window.location.reload();
                } else {
                  alert('Create Event Draggable failed');
                }

              },
              error : function() {
               alert('Connection Failed');
             }
           });

        //Create events
        var event = $('<div />')
        event.css({
          'background-color': currColor,
          'border-color'    : currColor,
          'color'           : '#fff'
        }).addClass('external-event')
        event.html(val)
        $('#external-events').prepend(event)

        //Add draggable funtionality
        init_events(event)

        //Remove event from text input
        $('#new-event').val('')
      })

    // initialize datepicker
    $(".datepicker").datepicker({
      format: 'dd MM yyyy',
      startDate: 'd',
      todayBtn: 'linked',
      todayHighlight: true
    });

    // initialize timepicker
    $(".timepicker").timepicker({
      defaultTime: '08:00',
      showMeridian: false,
    });

    // modal add or edit color chooser
    /* ADDING EVENTS */
    var currColorModal = '#00a65a' //Green by default
    
    $('#color-chooser-modal > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColorModal = $(this).css('color')
      //Add color effect to button

      var modalHeader = $(this).closest('.modal').find('.modal-header');
      var modalButton = $(this).closest('.modal').find('#btn-submit');
      modalHeader.css({ 'background-color': currColorModal, 'border-color': currColorModal });
      modalButton.css({ 'background-color': currColorModal, 'border-color': currColorModal });

      var inputBackgroundColor = $(this).closest('.modal').find('#backgroundColor');
      var inputBorderColor = $(this).closest('.modal').find('#borderColor');
      inputBackgroundColor.val(currColorModal);
      inputBorderColor.val(currColorModal);
    })

    // Clear Modal On Hidden
    $('.modal').on('hidden.bs.modal', function () {
      $(this).find("input[type='text'],textarea,select").val('').end();
    });

    $(".btn-update-submit").click(function(e){

      if ($('#event-remove').is(':checked')) {

        if (!confirm('Apakah Anda yakin menghapus schedule ini ?')) {
          e.preventDefault();
        }

      }
      
    });

     //Initialize Select2 Elements
     $('.select2').select2()


     // participant email onchange
     function add_to_participant_hidden(participant_select, participant_email_hidden){
        var participant_array = participant_select.val();
        var participant_string = participant_array.join();
        alert(participant_string);
     }

     /*$('#addModal #participant').change(function(){
        add_to_participant_hidden( $(this), $('.participant_email') );
     });

     $('#editModal #participant').change(function(){
        add_to_participant_hidden( $(this), $('.participant_email2') );
     });*/

   })

