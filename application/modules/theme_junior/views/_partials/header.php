<!-- Header -->
<header id="header" class="jnr__header header--one clearfix">
 <!-- Start Header Top Area -->
 <div class="junior__header__top">
    <div class="container">
       <div class="row">
          <div class="col-md-7 col-lg-6 col-sm-12">
             <div class="jun__header__top__left">
                <ul class="top__address d-flex justify-content-start align-items-center flex-wrap flex-lg-nowrap flex-md-nowrap">
                   <li><i class="fa fa-envelope"></i><a href="#">junior@mail.com</a></li>
                   <li><i class="fa fa-phone"></i><span>Contact Now :</span><a href="#">+003457289</a></li>
               </ul>
           </div>
       </div>
       <div class="col-md-5 col-lg-6 col-sm-12">
         <div class="jun__header__top__right">
            <ul class="accounting d-flex justify-content-lg-end justify-content-md-end justify-content-start align-items-center">
               <li><a class="login-trigger" href="#">Login</a></li>
               <li><a class="accountbox-trigger" href="#">Register</a></li>
           </ul>
       </div>
   </div>
</div>
</div>
</div>
<!-- End Header Top Area -->
<!-- Start Mainmenu Area -->
<div class="mainmenu__wrapper bg__cat--1 poss-relative header_top_line sticky__header">
    <div class="container">
       <div class="row d-none d-lg-flex">
          <div class="col-sm-4 col-md-6 col-lg-2 order-1 order-lg-1">
             <div class="logo">
                <a href="index.html">
                   <img src="<?=$current_theme ?>images/logo/junior.png" alt="logo images">
               </a>
           </div>
       </div>
       <div class="col-sm-4 col-md-2 col-lg-9 order-3 order-lg-2">
         <div class="mainmenu__wrap">
            <nav class="mainmenu__nav">
                <ul class="mainmenu">
                    <li class="drop"><a href="index.html">Home</a>
                        <ul class="dropdown__menu">
                            <li><a href="home">home version 01</a></li>
                            <li><a href="home/second">home version 02</a></li>
                            <li><a href="home/third">home version 03</a></li>
                            <li><a href="home/fourth">home version 04</a></li>
                            <li><a href="home/fifth">home version 05</a></li>
                        </ul>
                    </li>
                    <li class="drop"><a href="class">Class</a>
                        <ul class="dropdown__menu">
                            <li><a href="class">Class Grid</a></li>
                            <li><a href="class/list">Class List</a></li>
                            <li><a href="class/detail">Class Details</a></li>
                            <li><a href="class/leftSidebar">Class Details LeftSidebar</a></li>
                            <li><a href="class/rightSidebar">Class Details RightSidebar</a></li>
                        </ul>
                    </li>
                    <li class="drop"><a href="event">Event</a>
                        <ul class="dropdown__menu">
                            <li><a href="event/grid">Event Grid</a></li>
                            <li><a href="event/list">Event list</a></li>
                            <li><a href="event/details">Event Details</a></li>
                        </ul>
                    </li>
                    <li class="drop"><a href="#">Pages</a>
                        <ul class="dropdown__menu">
                            <li><a href="pages/aboutUs">about us</a></li>
                            <li><a href="pages/service">our service</a></li>
                            <li><a href="pages/gallery/one">gallery One</a></li>
                            <li><a href="pages/gallery/two">gallery Two</a></li>
                            <li><a href="pages/gallery/three">gallery Three</a></li>
                            <li><a href="pages/gallery/details">gallery Details</a></li>
                            <li><a href="pages/cart">cart Page</a></li>
                            <li><a href="pages/wishlist">wishlist page</a></li>
                            <li><a href="pages/checkout">checkout page</a></li>
                        </ul>
                    </li>
                    <li class="drop"><a href="shop">Shop</a>
                        <ul class="dropdown__menu">
                            <li><a href="shop">Shop Grid</a></li>
                            <li><a href="shop/gridLeftSidebar">Shop Grid Left Sidebar</a></li>
                            <li><a href="shop/gridRightSidebar">Shop Grid right Sidebar</a></li>
                            <li><a href="shop/single">Shop Single</a></li>
                            <li><a href="shop/singleSidebar">Shop Single Sidebar</a></li>
                        </ul>
                    </li>
                    <li class="drop"><a href="blog">Blog</a>
                        <ul class="dropdown__menu">
                            <li><a href="blog">Blog Grid</a></li>
                            <li><a href="blog/list">Blog List</a></li>
                            <li><a href="blog/listRightSidebar">Blog List RightSidebar</a></li>
                            <li><a href="blog/listLeftSidebar">Blog List LeftSidebar</a></li>
                            <li><a href="blog/details">Blog Details</a></li>
                        </ul>
                    </li>
                    <li><a href="blog/contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
     <div class="shopbox d-flex justify-content-end align-items-center">
        <a class="minicart-trigger" href="#">
           <i class="fa fa-shopping-basket"></i>
       </a>
       <span>03</span>
   </div>
</div>
</div>
<!-- Mobile Menu -->
<div class="mobile-menu d-block d-lg-none">
   <div class="logo">
      <a href="index.html"><img src="<?=$current_theme ?>images/logo/junior.png" alt="logo"></a>
  </div>
  <a class="minicart-trigger" href="#">
      <i class="fa fa-shopping-basket"></i>
  </a>
</div>
<!-- Mobile Menu -->
</div>
</div>
<!-- End Mainmenu Area -->
</header>
		<!-- //Header -->