<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'Global Prestasi School',

	// google api key
	'google_api_key' => 'AIzaSyAshH-WsVH1fx2b0nNdnqH-iSpWu66N9N4',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			'src/themes/two/plugins/jquery/jquery-1.11.1.min.js',
		),
		'foot'	=> array(
			'src/themes/two/plugins/bootstrap/js/bootstrap.min.js',
			'src/plugins/colorbox/jquery.colorbox-min.js',
			'src/themes/two/plugins/flexslider/jquery.flexslider.js',
			'src/themes/two/plugins/rs-plugin/js/jquery.themepunch.tools.min.js',
			'src/themes/two/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js',
			'src/themes/two/plugins/selectbox/jquery.selectbox-0.1.3.min.js',
			'src/themes/two/plugins/pop-up/jquery.magnific-popup.js',
			'src/themes/two/plugins/animation/waypoints.min.js',
			'src/themes/two/plugins/count-up/jquery.counterup.js',
			'src/themes/two/plugins/animation/wow.min.js',
			'src/themes/two/plugins/animation/moment.min.js',
			'src/themes/two/plugins/calender/fullcalendar.min.js',
			'src/themes/two/plugins/owl-carousel/owl.carousel.js',
			'src/themes/two/plugins/timer/jquery.syotimer.js',
			'src/js/custom.js'
			
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'src/themes/two/plugins/bootstrap/css/bootstrap.min.css',
			'src/plugins/colorbox/colorbox.css',
			'src/themes/two/plugins/selectbox/select_option1.css',
			'src/themes/two/plugins/font-awesome/css/font-awesome.min.css',
			'src/themes/two/plugins/flexslider/flexslider.css',
			'src/themes/two/plugins/calender/fullcalendar.min.css',
			'src/themes/two/plugins/animate.css',
			'src/themes/two/plugins/pop-up/magnific-popup.css',
			'src/themes/two/plugins/rs-plugin/css/settings.css',
			'src/themes/two/plugins/owl-carousel/owl.carousel.css',
			'https://fonts.googleapis.com/css?family=Open+Sans:400,600,600italic,400italic,700',
			'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700',
			'src/css/frontend.css'
		)
	),

	// Default CSS class for <body> tag
	'body_class' => 'body-wrapper version3',
	
	// Multilingual settings
	'languages' => array(
		/*'default'		=> 'en',
		'autoload'		=> array('general'),
		'available'		=> array(
			'en' => array(
				'label'	=> 'English',
				'value'	=> 'english'
			),
			'zh' => array(
				'label'	=> '繁體中文',
				'value'	=> 'traditional-chinese'
			),
			'cn' => array(
				'label'	=> '简体中文',
				'value'	=> 'simplified-chinese'
			),
			'es' => array(
				'label'	=> 'Español',
				'value' => 'spanish'
			)
		)*/
	),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => '',

	// Restricted pages
	'page_auth' => array(
	),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',
		
		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_frontend';