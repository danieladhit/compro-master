<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['admin'] 						= 'dashboard';


/*$route['cms/banner'] 			= "cms/banner";
$route['cms/berita'] 			= "cms/berita";
$route['cms/blog'] 			= "cms/blog";
$route['cms/post'] 			= "cms/blog";
$route['cms/category'] 		= "cms/category";
$route['cms/dashboard'] 		= "cms/dashboard";
$route['cms/fitur'] 			= "cms/fitur";
$route['cms/galeri'] 			= "cms/galeri";
$route['cms/galeri_category'] = "cms/galeri_category";
$route['cms/karya'] 			= "cms/karya";
// $route['cms/klien'] 			= "cms/klien";
$route['cms/kursus'] 			= "cms/kursus";
$route['cms/liputan'] 		= "cms/liputan";
$route['cms/menu'] 			= "cms/menu";
$route['cms/page'] 			= "cms/page";
$route['cms/slider'] 			= "cms/slider";
$route['cms/subcategory'] 	= "cms/subcategory";
$route['cms/testimonial'] 	= "cms/testimonial";
$route['cms/ticker'] 			= "cms/ticker";
$route['cms/user'] 			= "cms/user";
$route['cms/user_admin'] 		= "cms/user_admin";
$route['cms/video'] 			= "cms/video";
$route['cms/video_category'] 	= "cms/video_category";
$route['cms/kursus'] 			= "cms/kursus";
$route['cms/message'] 		= "cms/message";
$route['cms/setting'] 		= "cms/setting";
$route['cms/password']		= "cms/password";

$route['cms/banner/(:any)'] 			= "cms/banner/$1";
$route['cms/berita/(:any)'] 			= "cms/berita/$1";
$route['cms/blog/(:any)'] 			= "cms/blog/$1";
$route['cms/post/(:any)'] 			= "cms/blog/$1";    // post & blog satu yang sama 
$route['cms/category/(:any)'] 		= "cms/category/$1";
$route['cms/fitur/(:any)'] 			= "cms/fitur/$1";
$route['cms/dashboard/(:any)'] 		= "cms/dashboard/$1";
$route['cms/galeri/(:any)'] 			= "cms/galeri/$1";
$route['cms/galeri_category/(:any)'] 	= "cms/galeri_category/$1";
$route['cms/karya/(:any)'] 			= "cms/karya/$1";
$route['cms/klien/(:any)'] 			= "cms/klien/$1";
$route['cms/kursus/(:any)'] 			= "cms/kursus/$1";
$route['cms/liputan/(:any)'] 			= "cms/liputan/$1";
$route['cms/menu/(:any)'] 			= "cms/menu/$1";
$route['cms/page/(:any)'] 			= "cms/page/$1";
$route['cms/slider/(:any)'] 			= "cms/slider/$1";
$route['cms/subcategory/(:any)'] 		= "cms/subcategory/$1";
$route['cms/testimonial/(:any)'] 		= "cms/testimonial/$1";
$route['cms/ticker/(:any)'] 			= "cms/ticker/$1";
$route['cms/user/(:any)'] 			= "cms/user/$1";
$route['cms/user_cms/(:any)'] 		= "cms/user_cms/$1";
$route['cms/video/(:any)'] 			= "cms/video/$1";
$route['cms/video_category/(:any)'] 	= "cms/video_category/$1";
$route['cms/kursus/(:any)'] 			= "cms/kursus/$1";
$route['cms/message/(:any)'] 			= "cms/message/$1";
$route['cms/setting/(:any)'] 			= "cms/setting/$1";
$route['cms/log/log_user'] 			= "cms/log/log_user";
$route['cms/log/log_user_activity']	= "cms/log/log_user_activity";
$route['cms/log/log_visitor'] 		= "cms/log/log_visitors";
$route['cms/log'] 					= "cms/log";
$route['cms/klien'] 					= "cms/klien";
$route['cms/klien/(:any)'] 			= "cms/klien/$1";
$route['cms/password/(:any)']			= "cms/password/$1";

$route['cms/inbox'] 					= "cms/inbox";

$route['(:any)'] 						= 'page/detail';
$route['(:any)/(:any)']					= 'page/detail';
$route['(:any)/(:any)/(:any)']			= 'page/detail';*/
// $route['(:any)/(:any)/(:any)/(:any)']	= 'page/detail';
