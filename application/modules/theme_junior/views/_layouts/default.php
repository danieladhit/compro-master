<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
	<?php $this->load->view("_partials/header"); ?>
	<?php //$this->load->view('_partials/navbar'); ?>

	<?php $this->load->view($inner_view); ?>

	<?php $this->load->view('_partials/footer'); ?>

	<?php $this->load->view('_partials/hidden'); ?>

</div><!-- //Main wrapper -->