 <div class="brandSection clearfix" style="padding: 20px 0px">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="owl-carousel partnersLogoSlider">
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 50%; height: auto;" src="src/images/diknas.png" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 50%; height: auto;" src="src/images/GEGBekasi.png" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 100%; height: auto;" src="src/images/logo_cambridge.jpg" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 100%; height: auto;" src="src/images/logo_cambridge2.png" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 70%; height: auto;" src="src/images/logo-gp-kids.png" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 35%; height: auto;" src="src/images/logo-harapan-mulia.png" /></a>
            </div>
          </div>
          <div class="slide">
            <div class="partnersLogo clearfix">
              <a href="#"><img style="width: 100%; height: auto;" src="src/images/logo-montes.png" /></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- Brand-section -->

<footer class="footer-v2 footer-v3">
  <div class="menuFooter clearfix">
    <div class="container">
      <div class="row clearfix">

        <!-- <div class="col-sm-12 col-xs-12">
          <div class="footer-about">
            <h5>Histori</h5>
            <p>Global Prestasi School (GPS) yang terdiri dari kelompok bermain(KB) Taman Kanak-kanak(TK) Sekolah Dasar(SD) Sekolah Menengah Pertama(SMP) Sekolah Menengah Atas (SMA) di selenggarakan oleh Yayasan Harapan Global Mandiri (YHGM). SMP-SMA Global Prestasi School mulai beroperasi ditahun 2005, sedangkan SD Global Prestasi ditahun 2007. sekolah SMP dan SMA ditetapkan sebagai sekolah model oleh dinas pendidikan dan mendapat status akreditasi "A" dari badan akreditasi provinsi jawa barat ditahun 2007.</p>
            <a href="">Read More &gt;</a>
          </div>
        </div>col-sm-3 col-xs-6 -->

        <div class="col-sm-4 col-xs-6">
          <h5>ADMISSION OFFICE</h5>
          <div class="footer-contact">
            <ul>
              <li> <i class="fa fa-home" aria-hidden="true"></i>Komplek GPS Jl.K.H Noer Alie No.10 B
                Bekasi Jawa Barat 17145</li>
                <li><i class="fa fa-phone" aria-hidden="true"></i>021-8885 2668 / 885 4311</li>
                <li>021 888 50573 & 021 888 505 82 (fax) - GPS Montessori</li>
                <li><i class="fa fa-whatsapp" aria-hidden="true"></i>0819 0888 3385</li>
                <li><a href="mailto:info@globalprestasi.sch.id"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@globalprestasi.sch.id</a></li>
              </ul>
            </div>
          </div><!-- col-sm-4 col-xs-6 -->

          <div class="col-sm-4 col-xs-6">
            <h5>Harapan Mulia Elementary School</h5>
            <div class="footer-contact">
              <ul>
                <li> <i class="fa fa-home" aria-hidden="true"></i>Jl.Gardenia 1 Kav 21 -24 Villa Galaxy - Bekasi</li>
                <li><i class="fa fa-phone" aria-hidden="true"></i>021-824 02776</li>
                <li><a href="mailto:info@globalprestasi.sch.id"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@globalprestasi.sch.id</a></li>
              </ul>
            </div>
          </div><!-- col-sm-4 col-xs-6 -->

          <div class="col-sm-4 col-xs-6">
            <h5>Global Prestasi Kids</h5>
            <div class="footer-contact">
              <ul>
                <li> <i class="fa fa-home" aria-hidden="true"></i>Pondok Kelapa Indah B2/1 Jakarta Timur
                  13810</li>
                  <li><i class="fa fa-phone" aria-hidden="true"></i>021-864 1603</li>
                  <li><a href="mailto:info@globalprestasi.sch.id"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@globalprestasi.sch.id</a></li>
                </ul>
              </div>
            </div><!-- col-sm-4 col-xs-6 -->

          </div><!-- row clearfix -->
        </div><!-- container -->
      </div><!-- menuFooter -->

      <div class="footer clearfix">
        <div class="container">
          <div class="footer-bottom">
            <div class="row clearfix">
              <div class="col-sm-6 col-xs-12 copyRight">
                <p>© <?=date('Y') ?> Copyright Global Prestasi School</p>
              </div><!-- col-sm-6 col-xs-12 -->
              <div class="col-sm-6 col-xs-12 privacy_policy">

              </div><!-- col-sm-6 col-xs-12 -->
            </div><!-- row clearfix -->
          </div>
        </div><!-- container -->
      </div><!-- footer -->
    </footer>

    