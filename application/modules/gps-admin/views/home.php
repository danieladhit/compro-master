<h3>Admission Info</h3>
<div class="row">
	<div class="col-md-4">
		<?php echo modules::run('adminlte/widget/info_box', 'yellow', $count['admission'], 'Online Registration', 'ion ion-ios-people-outline', 'admission'); ?>
	</div>
	<div class="col-md-4">
		<?php echo modules::run('adminlte/widget/info_box', 'blue', $count['poster_view'], 'Poster View', 'ion ion-ios-eye-outline', 'admission/poster_view'); ?>
	</div>
</div>
