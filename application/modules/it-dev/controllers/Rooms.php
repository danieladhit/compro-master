<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('calendar_rooms');
		$crud->columns('name', 'description');
		$crud->fields('name', 'description');

		$this->mPageTitle = 'Manage Rooms';
		$this->render_crud();
	}

}
