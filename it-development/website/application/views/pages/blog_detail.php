<div class="post_section clearfix">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 post_left">
				<div class="post_left_section">
					<div class="post_left_section post_left_border">
						<div class="post single_post">
							<div class="post_thumb">
								<img src="<?=base_url('uploads/blog/'.$blog->blog_picture) ?>" alt="">
							</div><!--end post thumb-->
							<div class="meta">
								<span class="author">By: <a href="#"><?=$blog->username ?></a></span>
								<span class="category"> <a href="#"><?=$blog->category_title ?></a></span>
								<span class="date">Posted: <a href="#"><?=date('F d, Y', strtotime($blog->create_date)) ?></a></span>
							</div><!--end meta-->
							<h1><?=$blog->blog_title ?></h1>
							<div class="post_desc">
								<?=$blog->blog_description ?>
							</div><!--end post desc-->
							<!-- <div class="post_bottom">
								<ul>
									<li class="like">
										<a href="#">
											<img src="img/news/like_icon.png" alt="">
											<span>12</span>
										</a>
									</li>
									<li class="share">
										<a href="#">
											<img src="img/news/share_icon.png" alt="">
											<span>12</span>
										</a>
									</li>
									<li class="favorite">
										<a href="#">
											<img src="img/news/favorite_icon.png" alt="">
											<span>12</span>
										</a>
									</li>
								</ul>
							</div> --><!--end post bottom-->
						</div><!--end post-->
						<div class="related_post_sec single_post">
							<h3>Related Posts</h3>
							<ul>
								<?php foreach($blog_related as $key => $row){ ?>
								<li>
									<span class="rel_thumb">
										<a href="<?=base_url('blog/'.$row->blog_slug) ?>"><img src="<?=base_url('uploads/blog/'.$row->blog_picture) ?>" alt=""></a>
									</span><!--end rel_thumb-->
									<div class="rel_right">
										<h4><a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=character_limiter($row->blog_title, 45) ?></a></h4>
										<div class="meta">
											<span class="author">By: <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->username ?></a></span>
											<span class="category"> <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->category_title ?></a></span>
											<span class="date">Posted: <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=date('F d, Y', strtotime($row->create_date)) ?></a></span>
										</div>
										<p><?=character_limiter($row->blog_description, 200) ?></p>
									</div><!--end rel right-->
								</li>
								<?php } ?>
							</ul>
						</div><!--end single_post-->

						<!-- <div class="comments_section">
							<div class="comment_post">
								<h3>Comments</h3>
								<div class="comment_header">
									<ul>
										<li class="comment_count">1 comment</li>
										<li class="comment_favorite_count"><i class="fa fa-star"></i> <span>0</span></li>
									</ul>
								</div>end comment header
								<form class="reply" action="#" method="post">
									<div class="postbox">
										<div class="avatar">
											<span class="user">
												<img alt="Avatar" src="img/news/avatar.png">
											</span>
										</div>
										<div class="comments_field">
											<input class="form-control comments" placeholder="Leave a message..." type="text">
										</div>end comments field
									</div>end postbox
									<div id="post_list">
										<div class="comment_tools">
											<ul>
												<li class="sort"><a href="#">Best <i class="fa fa-caret-down"></i></a></li>
												<li class="community"><a href="#">Community</a></li>
												<li class="setting"><a href="#"><img src="img/news/setting.png" alt=""> <i class="fa fa-caret-down"></i></a></li>
												<li class="share"><a href="#">Share <i class="fa fa-share-square-o"></i></a></li>
											</ul>
										</div>end comment tools
										<div class="post-content" data-role="post-content">
											<div class="avatar">
												<span class="user">
													<img alt="Avatar" src="img/news/avatar.png">
												</span>
											</div>
											<div class="post-body">
												<div class="post-top">
													<span class="post-byline">
														<span class="author publisher-anchor-color"><a href="#">Gavin Hoffman</a></span>
													</span>
													<span class="post-meta">
														<a href="#">5 hours ago</a>
													</span>
												</div>end post-top
												<div class="post-body-inner">
													<div class="post-message">
														<p>Good</p>
													</div>
												</div>end post-body-inner
												<ul class="comment_footer">
													<li data-role="voting" class="voting">
														<a href="#">
															<span class="control left"><i class="fa fa-angle-up"></i></span>
														</a>
														<span title="Vote down" data-action="downvote" class="vote-down  count-1" role="button">
						
															<span class="control"><i class="fa fa-angle-down"></i></span>
														</span>
													</li>
													<li data-role="reply-link" class="reply">
														<a data-action="reply" href="#">Reply</a>
													</li>
													<li class="share">
														<a class="toggle"><span class="text">Share <i class="fa fa-angle-right"></i></span></a>
													</li>
												</ul>
											</div>end post-body
										</div>end post-content
									</div>end post_list
								</form>
								<div class="comment_bottom_block">
									<ul>
										<li><a href="#"><i class="fa fa-rss"></i> &nbsp; Comment feed</a></li>
										<li><a href="#"><i class="fa fa-envelope-o"></i> &nbsp; Subscribe via email</a></li>
									</ul>
								</div>end comment_bottom_block
							</div>end comment post
							<div class="comments_form">
								<h3>Post A Comment</h3>
								<form action="#" method="post">
									<div class="half">
										<input class="form-control" placeholder="Name" type="text">
									</div>
									<div class="half right">
										<input class="form-control" placeholder="Email" type="text">
									</div>
									<div class="full">
										<textarea rows="9" cols="10" class="form-control" placeholder="Write a comment"></textarea>
									</div>
									<input class="commonBtn" value="Submit" type="submit">
								</form>
							</div>end comments form
						</div> --><!--end comments section-->
					</div>
				</div><!--end post left section-->
			</div><!--end post_left-->

			<div class="col-xs-12 col-sm-4 post_right">
				
				<?php include "sidebar.php"; ?>

			</div><!--end post_right-->

		</div>
	</div>
</div>