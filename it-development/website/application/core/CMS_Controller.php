<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set ('Asia/Jakarta');
/*===============================================
	3.	LOG VISITOR
	4.	SEND EMAIL
	6.	MINIFY
===============================================*/
class CMS_Controller extends MX_Controller {


	public function __construct(){
		parent::__construct();
	
	/*===============================================
			2.	SET THEMES NAME 
		===============================================*/	
		$template = $this->db->query("SELECT template_name FROM templates WHERE active='1'")->row();
		$this->themes_name = $template->template_name;
		$this->themes_folder_name = 'company_profile_template/'.$this->themes_name;


		/*===============================================
			3.	SET FRONT END TEMPLATE 
		===============================================*/
		$this->front_end_template = 'main/'.$this->themes_folder_name.'/template';

		/*===============================================
			4.	SETTING TEMPLATE 
		===============================================*/
		
		$settingnya_template		= $this->model_utama->get_detail('1','setting_id','setting')->row();
		
		$this->meta_description		= $settingnya_template->website_meta_description;
		$this->analytics		= $settingnya_template->analytics;
		$this->chat		= $settingnya_template->chat;
		
		/*===============================================
			5.	SETTING Fitur 
		===============================================*/
		
		$settingnya_fitur		= $this->model_utama->get_detail('1','setting_home_id','setting_home')->row();
		
		$this->galeri		= $settingnya_fitur->gallery;
	}	
	
	/*===============================================
		3.	LOG VISITOR
	===============================================*/
	function log_visitor($action){
		$this->load->model('model_utama','',TRUE);
		$this->load->helper('date');
		$this->load->library('user_agent');
		$table 		= 'log_visitor';
		$array_data = 	array(
							'ip_address'		=> 	$_SERVER['HTTP_USER_AGENT'],
							'activity'			=> 	$action,
							'browser'			=> 	$this->agent->browser(), 
  							'version'			=> 	$this->agent->version(), 
							'mobile'			=> 	$this->agent->mobile(), 
							'robot'				=>	$this->agent->robot(),
							'platform'			=>  $this->agent->platform(),
							'create_date'		=> 	date('Y-m-d H:i:s',now()),
							'user_id'			=> 	$this->session->userdata('user_id')
							);
		$query = $this->model_utama->insert_data($table,$array_data);
	}
	
	
	/*===============================================
		4.	SEND EMAIL
	===============================================*/
	function send_email($from,$recipient,$subject,$message){
		
		$config = Array(
						'mailtype'  => 'html'
						);
		
		$this->load->library('email',$config);
		$this->email->from($from, 'Babastudio');
		$this->email->to($recipient);
		$this->email->subject($subject);
		
		$this->email->message($message);
		
		
		if( $this->email->send() ){
			return true;	
		}else{
			return false;	
		}
	}

	/*===============================================
		6.	MINIFY
	===============================================*/
	public function minify($empty_cache=false){
		
		/*====================================================
			1.	LOAD LIBRARY
		====================================================*/
		$this->load->library('carabiner');
		
		$carabiner_config = array(
								'script_dir' => 'assets/front_page/', 
								'style_dir'  => 'assets/front_page/',
								'cache_dir'  => 'assets/cache/',
								'base_uri'	 => base_url(),
								'dev' 		 => FALSE,
								'combine'	 => false,
								'minify_js'  => false,
								'minify_css' => false
							);
		$this->carabiner->config($carabiner_config);
		
		/*====================================================
			2. 	CLEAR CACHE
		====================================================*/
		if( $empty_cache ){
			$this->carabiner->empty_cache();
		}
		
		/*====================================================
			3.	MINIFY CSS
		====================================================*/
		/*$this->carabiner->css('plugins/bootstrap/css/bootstrap.min.css');
		$this->carabiner->css('plugins/font-awesome/css/font-awesome.css');
		$this->carabiner->css('plugins/pe-icon-7-stroke/css/pe-icon-7-stroke.css');

		$this->carabiner->css('plugins/animate-css/animate.min.css');
		$this->carabiner->css('plugins/flexslider/flexslider.css');
		$this->carabiner->css('css/styles-3.css');
		
		$this->carabiner->css('css/custom.css');
		$this->carabiner->css('dist/css/lightbox.min.css');

		$this->carabiner->css('plugins/carousel/owl.carousel.css');*/
	
		/*====================================================
			4.	MINIFY JS
		====================================================*/
		/*$this->carabiner->js('plugins/jquery-1.12.3.min.js');
		$this->carabiner->js('plugins/bootstrap/js/bootstrap.min.js');
		$this->carabiner->js('plugins/bootstrap-hover-dropdown.min.js');	
		$this->carabiner->js('plugins/jquery-inview/jquery.inview.min.js');
		$this->carabiner->js('plugins/isMobile/isMobile.min.js');
		$this->carabiner->js('plugins/back-to-top.js');
		$this->carabiner->js('plugins/jquery-placeholder/jquery.placeholder.js');
		$this->carabiner->js('plugins/FitVids/jquery.fitvids.js');
		$this->carabiner->js('plugins/flexslider/jquery.flexslider-min.js');
		$this->carabiner->js('js/main.js');

		$this->carabiner->js('plugins/carousel/owl.carousel.min.js');

		$this->carabiner->js('bttrlazyload.js');
		$this->carabiner->js('bttrlazyload-init.js');*/

		// ie fix
		// $array_js 	= array('ie/html5shiv.min.js','ie/respond.min.js','ie/selectivizr-min.js');
		// $array_css 	= array();
		// $this->carabiner->group('iefix',array('js' => $array_js, 'css' => $array_css));


		/*====================================================
			5.	DISPLAY CSS / JS
				ON , VIEW
				<?php echo $this->carabiner->display('css'); ?>
				<?php echo $this->carabiner->display('js'); ?>
				
				result
				=====================
				<link ... />
		====================================================*/
	}
	
	function upload_photo( 	$folder_path,
							$config,
							$width = '350'
	){
		/*===============================================================
			1.	CREATE DIRECTORY
		===============================================================*/
		$all_path = explode('/',$folder_path);
		$x 		= 0;
		$path 	= '';
		foreach( $all_path as $fp ){
			$separator = '/';
			if( $x == 0 ){
				$separator = '';
			}
			$path = $path . $separator . $fp; 	
			if (!is_dir($path)){
		        mkdir('./'.$path.'/', 0777, true);
		    }
		    $x++;
		}

	    /*===============================================================
			2.	UPLOAD PHOTO
		===============================================================*/	
	    $this->load->library('upload', $config);
		if ( ! $this->upload->do_upload())
		{
			$file_dokumen	= '';
		}
		else
		{
			/*===============================================================
				2.	RESIZE IMAGE
			===============================================================*/	
			$dokumen		= $this->upload->data();
			$file_dokumen	= $dokumen['file_name'];

			$this->load->library('image_lib');
			$config_resize['image_library'] 	= 'gd2';
			$config_resize['source_image']		= $dokumen['full_path'];
			$config_resize['new_image']			= './'.$folder_path;
			$config_resize['create_thumb'] 		= TRUE;
			$config_resize['maintain_ratio'] 	= TRUE;
			$config_resize['thumb_marker'] 		= '';
			$config_resize['width']	 			= $width;
			$config_resize['quality'] 			= "100%";
			//$dim 								= (intval($dokumen["image_width"]) / intval($dokumen["image_height"])) - ($config_resize['width'] / $config_resize['height']);
			//$config_resize['master_dim'] 		= ($dim > 0)? "height" : "width";
			$this->image_lib->initialize($config_resize);
			if(!$this->image_lib->resize()){ //Resize image
			    echo $this->image_lib->display_errors();
			    exit;
			}
		}
		return $file_dokumen;
	}
	
}