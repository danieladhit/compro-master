<!-- Strat Slider Area -->
<div class="slide__carosel owl-carousel owl-theme">
	<div class="slider__area slider--three bg-image--11 d-flex fixed--height justify-content-center align-items-center poss-relative">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="slider__activation">
						<!-- Start Single Slide -->
						<div class="slide">
							<div class="slide__inner">
								<h1>Play & learn How to</h1>
								<h4>Create New Things</h4>
								<div class="slider__btn">
									<a class="dcare__btn btn__org max__height-btn hover--theme" href="#">Read More</a>
								</div>
							</div>
						</div>
						<!-- End Single Slide -->
					</div>
				</div>
			</div>
		</div>
		<div class="slider__btm__img-1">	
			<img src="<?=$current_theme ?>images/slider/1.png" alt="slider images">
		</div>
		<div class="slider__btm__img-2">	
			<img src="<?=$current_theme ?>images/slider/2.png" alt="slider images">
		</div>
	</div>
	<div class="slider__area slider--three bg-image--11 d-flex fixed--height justify-content-center align-items-center poss-relative">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="slider__activation">
						<!-- Start Single Slide -->
						<div class="slide">
							<div class="slide__inner">
								<h1>Play & learn How to</h1>
								<h4>Creat New Things</h4>
								<div class="slider__btn">
									<a class="dcare__btn btn__org max__height-btn hover--theme" href="#">Read More</a>
								</div>
							</div>
						</div>
						<!-- End Single Slide -->
					</div>
				</div>
			</div>
		</div>
		<div class="slider__btm__img-1">	
			<img src="<?=$current_theme ?>images/slider/1.png" alt="slider images">
		</div>
		<div class="slider__btm__img-2">	
			<img src="<?=$current_theme ?>images/slider/2.png" alt="slider images">
		</div>
	</div>
</div>
<!-- End Slider Area -->
<!-- Start Choose Us Area -->
<section class="dcare__choose__us__area section-padding--lg bg--white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line">Why Choose Us</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<!-- Start Single Choose Option -->
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="dacre__choose__option">
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__inner">
							<h4><a href="about-us.html">Language Lesson</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/1.png" alt="choose icon">
						</div>
					</div>
					<!-- End Single Choose -->
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__inner">
							<h4><a href="about-us.html">Meals Provided</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/2.png" alt="choose icon">
						</div>
					</div>
					<!-- End Single Choose -->
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__inner">
							<h4><a href="about-us.html">Transportation</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/3.png" alt="choose icon">
						</div>
					</div>
					<!-- End Single Choose -->
				</div>
			</div>
			<!-- End Single Choose Option -->
			<!-- Start Single Choose Option -->
			<div class="col-lg-4 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
				<div class="dacre__choose__option wow flipInX">
					<div class="choose__big__img">
						<img src="<?=$current_theme ?>images/choose/big-img/1.png" alt="choose images">
					</div>
				</div>
			</div>
			<!-- End Single Choose Option -->
			<!-- Start Single Choose Option -->
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="dacre__choose__option text__align--left">
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/4.png" alt="choose icon">
						</div>
						<div class="choose__inner">
							<h4><a href="about-us.html">Professional Teacher</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
					</div>
					<!-- End Single Choose -->
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/5.png" alt="choose icon">
						</div>
						<div class="choose__inner">
							<h4><a href="about-us.html">Smart Education</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
					</div>
					<!-- End Single Choose -->
					<!-- Start Single Choose -->
					<div class="choose">
						<div class="choose__icon">
							<img src="<?=$current_theme ?>images/choose/icon/6.png" alt="choose icon">
						</div>
						<div class="choose__inner">
							<h4><a href="about-us.html">Toys & Games</a></h4>
							<p>aincididunt ut labore et dolore magnaincidunt ut labore et dolore magnam aliquam qua</p>
						</div>
					</div>
					<!-- End Single Choose -->
				</div>
			</div>
			<!-- End Single Choose Option -->
		</div>
	</div>
</section>
<!-- End Choose Us Area -->
<!-- Start Popular Classa Area -->
<section class="dcare__popular__class__area bg-image--12 section-padding--lg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center white--title">
					<h2 class="title__line white__line">Our Popular Classes</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--60">
			<div class="col-md-12 ol-lg-12">
				<div class="popular__group__slide__init owl-carousel">
					<div class="popular__class__group">
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/1.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>30th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$50</span>
									</div>
								</div>
								<h6><a href="class-details.html">Color Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/2.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>30th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$50</span>
									</div>
								</div>
								<h6><a href="class-details.html">Alphabet Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
					</div>
					<div class="popular__class__group">
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/3.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>21th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$70</span>
									</div>
								</div>
								<h6><a href="class-details.html">Color Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/4.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>$20th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$40</span>
									</div>
								</div>
								<h6><a href="class-details.html">Alphabet Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
					</div>
					<div class="popular__class__group">
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/3.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>21th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$70</span>
									</div>
								</div>
								<h6><a href="class-details.html">Color Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
						<!-- Start Single Popular Class -->
						<div class="dacre__popular__classes">
							<div class="dc__popular__img">
								<a href="class-details.html">
									<img src="<?=$current_theme ?>images/popular-class/4.jpg" alt="popular images">
								</a>
								<div class="popular__class__hover">
									<a href="class-details.html">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div class="dc__popular__details">
								<div class="class__she">
									<div class="class__date">
										<span>$20th Dec, 2017</span>
									</div>
									<div class="class__price">
										<span>$40</span>
									</div>
								</div>
								<h6><a href="class-details.html">Alphabet Matching</a></h6>
								<p>Lor error sit volupta accusan laudantiab illnv ritatiarchitecto beatae viliq.</p>
								<ul class="class__size">
									<li>Age : 0.8 - 2.5 Years</li>
									<li>Class Size : 28</li>
								</ul>
							</div>
						</div>
						<!-- End Single Popular Class -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Popular Classa Area -->
<!-- Start Our Gallery Area -->
<section class="junior__gallery__area gallery__masonry__activation gallery--3 bg--white section-padding--md">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line">Our Gallery</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row galler__wrap masonry__wrap mt--50">
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.3s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/1.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.4s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/2.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.5s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/3.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.6s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/5.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.7s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/4.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
			<!-- Start Single Gallery -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12 gallery__item wow fadeInUp" data-wow-delay="0.8s">
				<div class="gallery">
					<div class="gallery__thumb">
						<a href="#">
							<img src="<?=$current_theme ?>images/gallery/gallery-maso/6.jpg" alt="gallery images">
						</a>
					</div>
					<div class="gallery__hover__inner">
						<div class="gallery__hover__action">
							<ul class="gallery__zoom">
								<li><a href="images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
								<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>	
			</div>	
			<!-- End Single Gallery -->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="gallery__btn text-center mt--80">
					<a class="dcare__btn btn__333333" href="#">View More</a>
				</div>
			</div>
		</div>	
	</div>
</section>
<!-- End Our Gallery Area -->
<!-- Start Call To Action -->
<section class="jnr__call__to__action call__to__action--2 bg-image--13">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="jnr__call__action__wrap d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between align-items-center">
					<div class="callto__action__inner">
						<h2>How to enroll your child to a class ?</h2>
						<p>This the right time to make your child life, Join our seminars & training.</p>
					</div>
					<div class="callto__action__btn">
						<a class="dcare__btn btn__org hover--theme" href="#">Find Out More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Call To Action -->
<!-- Start Product Area -->
<section class="dcare__product bg--white section-padding--lg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line">Our Product</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="product__nav nav justify-content-center" role="tablist">
					<a class="nav-item nav-link active" data-toggle="tab" href="#nav-all" role="tab">Children Cloth</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-shoe" role="tab">Children Shoes</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-food" role="tab">Baby food</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-toys" role="tab">Toys</a>
				</div>
			</div>
		</div>
		<div class="product__tab--content">
			<div class="row single__product__item tab-pane fade show active" id="nav-all" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Warm Cap</a></h4>
							<span>$60</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Gray Muffler</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-shoe" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Warm Cap</a></h4>
							<span>$60</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Gray Muffler</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-food" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Mixed Fruits Puree</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Mixed Fruits Puree</a></h4>
							<span>$60</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Mixed Fruits Puree</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Mixed Fruits Puree</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-toys" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Warm Cap</a></h4>
							<span>$60</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">Gray Muffler</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$50</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
		</div>
	</div>
</section>
<!-- End Product Area -->
<!-- Start Event Area -->
<section class="dcare__event__area bg-image--14 section-padding--lg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center white--title">
					<h2 class="title__line">Our Event</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--50">
			<!-- Start Single Event -->
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="dcare__event__wrapper">
					<!-- Start Event -->
					<div class="single__event d-flex">
						<div class="event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/sm-img/1.jpg" alt="event images">
							</a>
							<div class="event__hover__info">
								<span>16th Dec</span>
							</div>
						</div>
						<div class="event__inner">
							<h6><a href="event-details.html">Parents Day</a></h6>
							<ul class="event__time__location">
								<li><i class="fa fa-home"></i>Childrens Club, Uttara, Dhaka</li>
								<li><i class="fa fa-clock-o"></i>5.00 am to 9.00 pm</li>
							</ul>
						</div>
					</div>
					<!-- End Event -->
					<!-- Start Event -->
					<div class="single__event bg__light__green d-flex">
						<div class="event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/sm-img/1.jpg" alt="event images">
							</a>
							<div class="event__hover__info">
								<span>18th Dec</span>
							</div>
						</div>
						<div class="event__inner">
							<h6><a href="event-details.html">Children Day</a></h6>
							<ul class="event__time__location">
								<li><i class="fa fa-home"></i>Childrens Club, Uttara, Dhaka</li>
								<li><i class="fa fa-clock-o"></i>5.00 am to 9.00 pm</li>
							</ul>
						</div>
					</div>
					<!-- End Event -->
					<!-- Start Event -->
					<div class="single__event bg__baby__pink d-flex">
						<div class="event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/sm-img/1.jpg" alt="event images">
							</a>
							<div class="event__hover__info">
								<span>16th Dec</span>
							</div>
						</div>
						<div class="event__inner">
							<h6><a href="event-details.html">Drawing Competition</a></h6>
							<ul class="event__time__location">
								<li><i class="fa fa-home"></i>Childrens Club, Uttara, Dhaka</li>
								<li><i class="fa fa-clock-o"></i>5.00 am to 9.00 pm</li>
							</ul>
						</div>
					</div>
					<!-- End Event -->
				</div>
			</div>
			<!-- End Single Event -->
			<!-- Start Single Event -->
			<div class="col-lg-6 col-md-12 col-sm-12 mt--30">
				<div class="event__big__thumb owl-carousel">
					<!-- Start Single Img -->
					<div class="single__event__item">
						<div class="sin__event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/mid-img/1.jpg" alt="event images">
							</a>
						</div>
						<div class="sin__event__time">
							<p><i class="fa fa-calendar"></i>16th Dec, 2017</p>
						</div>
					</div>
					<!-- End Single Img -->
					<!-- Start Single Img -->
					<div class="single__event__item">
						<div class="sin__event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/mid-img/1.jpg" alt="event images">
							</a>
						</div>
						<div class="sin__event__time">
							<p><i class="fa fa-calendar"></i>16th Dec, 2017</p>
						</div>
					</div>
					<!-- End Single Img -->
					<!-- Start Single Img -->
					<div class="single__event__item">
						<div class="sin__event__thumb">
							<a href="event-details.html">
								<img src="<?=$current_theme ?>images/event/mid-img/1.jpg" alt="event images">
							</a>
						</div>
						<div class="sin__event__time">
							<p><i class="fa fa-calendar"></i>16th Dec, 2017</p>
						</div>
					</div>
					<!-- End Single Img -->
				</div>
			</div>
			<!-- End Single Event -->
		</div>
	</div>
</section>
<!-- End Event Area -->
<!-- Start Team Area -->
<section class="dcare__team__area section-padding--lg bg--white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line">Out Teachers</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<!-- Start Single Team -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="team__style--3">
					<div class="team__thumb">
						<img src="<?=$current_theme ?>images/team/big-img/1.jpg" alt="">
					</div>
					<div class="team__hover__action">
						<div class="team__details">
							<div class="team__info">
								<h6><a href="#">Hanchica Merica</a></h6>
								<span>Drawing Instructor</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
							<ul class="dacre__social__link--2 d-flex justify-content-center">
								<li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
								<li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
								<li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
								<li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Team -->
			<!-- Start Single Team -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="team__style--3">
					<div class="team__thumb">
						<img src="<?=$current_theme ?>images/team/big-img/2.jpg" alt="">
					</div>
					<div class="team__hover__action">
						<div class="team__details">
							<div class="team__info">
								<h6><a href="#">Najnin Supa</a></h6>
								<span>Drawing Instructor</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
							<ul class="dacre__social__link--2 d-flex justify-content-center">
								<li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
								<li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
								<li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
								<li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Team -->
			<!-- Start Single Team -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-12">
				<div class="team__style--3">
					<div class="team__thumb">
						<img src="<?=$current_theme ?>images/team/big-img/3.jpg" alt="">
					</div>
					<div class="team__hover__action">
						<div class="team__details">
							<div class="team__info">
								<h6><a href="#">Irin Pervin</a></h6>
								<span>Drawing Instructor</span>
							</div>
							<p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
							<ul class="dacre__social__link--2 d-flex justify-content-center">
								<li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
								<li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
								<li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
								<li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Team -->
		</div>
	</div>
</section>
<!-- End Team Area -->
<!-- Start Testimonial Area -->
<section class="dcare__testimonial__area section-padding--lg bg-image--15">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-lg-12">
				<div class="testimonial__activation tes__activation--2 owl-carousel owl-theme">
					<!-- Start Single Testimonial Area -->
					<div class="testimonial text-center">
						<div class="testimonial__icon">
							<img src="<?=$current_theme ?>images/testimonial/icon/2.png" alt="icon images">
						</div>
						<div class="testimonial__inner">
							<p>Lorem ipsum dolor t dolore magna aliqua. Ut enim ad minim veniam, quis nostexercitation ullamco laboris nisimollit anim id est lsunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatd.</p>
							<div class="tes__info">
								<h6>Lora alica</h6>
								<span>Gardients of student</span>
							</div>
						</div>
					</div>
					<!-- End Single Testimonial Area -->
					<!-- Start Single Testimonial Area -->
					<div class="testimonial text-center">
						<div class="testimonial__icon">
							<img src="<?=$current_theme ?>images/testimonial/icon/2.png" alt="icon images">
						</div>
						<div class="testimonial__inner">
							<p>Lorem ipsum dolor t dolore magna aliqua. Ut enim ad minim veniam, quis nostexercitation ullamco laboris nisimollit anim id est lsunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatd.</p>
							<div class="tes__info">
								<h6>Lora alica</h6>
								<span>Gardients of student</span>
							</div>
						</div>
					</div>
					<!-- End Single Testimonial Area -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Testimonial Area -->
<div class="subscribe__position">
	<!-- Start Kidz Area -->
	<div class="dacre__kidz__area section-padding--lg bg-image--16">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12">
					<div class="dcare__kidezone__wrapper d-flex justify-content-between">
						<!-- Start Single Kidz -->
						<div class="kidz__zone wow fadeInUp" data-wow-delay="0.3s">
							<div class="kidz__thumb">
								<img src="<?=$current_theme ?>images/funfact/fact-2/1.png" alt="kidz images">
							</div>
							<div class="kidz__details">
								<h6>Kidz <span>Zone</span></h6>
								<p>Gardients of student</p>
							</div>
						</div>
						<!-- End Single Kidz -->
						<!-- Start Single Kidz -->
						<div class="kidz__zone wow fadeInUp" data-wow-delay="0.4s">
							<div class="kidz__thumb">
								<img src="<?=$current_theme ?>images/funfact/fact-2/2.png" alt="kidz images">
							</div>
							<div class="kidz__details">
								<h6>Kidz <span>Zone</span></h6>
								<p>Gardients of student</p>
							</div>
						</div>
						<!-- End Single Kidz -->
						<!-- Start Single Kidz -->
						<div class="kidz__zone wow fadeInUp" data-wow-delay="0.5s">
							<div class="kidz__thumb">
								<img src="<?=$current_theme ?>images/funfact/fact-2/3.png" alt="kidz images">
							</div>
							<div class="kidz__details">
								<h6>Kidz <span>Zone</span></h6>
								<p>Gardients of student</p>
							</div>
						</div>
						<!-- End Single Kidz -->
						<!-- Start Single Kidz -->
						<div class="kidz__zone wow fadeInUp" data-wow-delay="0.6s">
							<div class="kidz__thumb">
								<img src="<?=$current_theme ?>images/funfact/fact-2/4.png" alt="kidz images">
							</div>
							<div class="kidz__details">
								<h6>Kidz <span>Zone</span></h6>
								<p>Gardients of student</p>
							</div>
						</div>
						<!-- End Single Kidz -->
						<!-- Start Single Kidz -->
						<div class="kidz__zone wow fadeInUp" data-wow-delay="0.7s">
							<div class="kidz__thumb">
								<img src="<?=$current_theme ?>images/funfact/fact-2/1.png" alt="kidz images">
							</div>
							<div class="kidz__details">
								<h6>Kidz <span>Zone</span></h6>
								<p>Gardients of student</p>
							</div>
						</div>
						<!-- End Single Kidz -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Kidz Area -->
	<!-- Start Subscribe Area -->
	<div class="bcare__subscribe subscribe--3">
		<div class="container bg-image--17">
			<div class="row">
				<div class="col-lg-12 col-sm-12 col-lg-12">
					<div class="subscribe__inner">
						<h2>Subscribe To Our Special Offers</h2>
						<div class="newsletter__form">
							<div class="input__box">
								<div id="mc_embed_signup">
									<form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
										<div id="mc_embed_signup_scroll" class="htc__news__inner">
											<div class="news__input">
												<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Enter Your E-mail" required>
											</div>
											<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
											<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
											<div class="clearfix subscribe__btn"><input type="submit" value="Send Now" name="subscribe" id="mc-embedded-subscribe" class="bst__btn btn--white__color">
											</div>
										</div>
									</form>
								</div>
							</div>        
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Subscribe Area -->
	<!-- Start Footer Area -->
	<footer id="footer" class="footer-area footer--2 space--vertical footer__variation--3">
		<div class="footer__wrapper bg__cat--8 section-padding--lg">
			<div class="container">
				<div class="row">
					<!-- Start Single Widget -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="footer__widget">
							<div class="ft__logo">
								<a href="index.html">
									<img src="<?=$current_theme ?>images/logo/junior.png" alt="logo images">
								</a>
							</div>
							<div class="ftr__address__inner">
								<div class="ftr__address">
									<div class="ftr_icon">
										<i class="fa fa-home"></i>
									</div>
									<div class="ftr__contact">
										<p>Uttara, Zamzam Tower</p>
										<p>Road # 12, Sec #13, 5th Floor</p>
									</div>
								</div>
								<div class="ftr__address">
									<div class="ftr_icon">
										<i class="fa fa-phone"></i>
									</div>
									<div class="ftr__contact">
										<p><a href="#">+08097-654321</a></p>
										<p><a href="#">+08097-654321</a></p>
									</div>
								</div>
								<div class="ftr__address">
									<div class="ftr_icon">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="ftr__contact">
										<p><a href="#">juniorhomeschool.@email.com</a></p>
										<p><a href="#">Kidscareschool.@yahoo.com</a></p>
									</div>
								</div>
								<div class="footer__social__icon">
									<ul class="dacre__social__link--2 d-flex justify-content-start">
										<li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
										<li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
										<li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Widget -->
					<!-- Start Single Widget -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12 xs-mt-40">
						<div class="footer__widget">
							<h4>Latest Blog</h4>
							<div class="footer__innner">
								<div class="ftr__latest__post">
									<!-- Start Single -->
									<div class="single__ftr__post d-flex">
										<div class="ftr__post__thumb">
											<a href="blog-details.html">
												<img src="<?=$current_theme ?>images/blog/post-img/2.jpg" alt="post images">
											</a>
										</div>
										<div class="ftr__post__details">
											<h6><a href="blog-details.html">Sports Day is near! so lets get ready soon</a></h6>
											<span><i class="fa fa-calendar"></i>30th Dec, 2017</span>
										</div>
									</div>
									<!-- End Single -->
									<!-- Start Single -->
									<div class="single__ftr__post d-flex">
										<div class="ftr__post__thumb">
											<a href="blog-details.html">
												<img src="<?=$current_theme ?>images/blog/post-img/3.jpg" alt="post images">
											</a>
										</div>
										<div class="ftr__post__details">
											<h6><a href="blog-details.html">Sports Day Celebration</a></h6>
											<span><i class="fa fa-calendar"></i>21th Dec, 2017</span>
										</div>
									</div>
									<!-- End Single -->
									<!-- Start Single -->
									<div class="single__ftr__post d-flex">
										<div class="ftr__post__thumb">
											<a href="blog-details.html">
												<img src="<?=$current_theme ?>images/blog/post-img/4.jpg" alt="post images">
											</a>
										</div>
										<div class="ftr__post__details">
											<h6><a href="blog-details.html">Sports Day Celebration</a></h6>
											<span><i class="fa fa-calendar"></i>10th Dec, 2017</span>
										</div>
									</div>
									<!-- End Single -->
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Widget -->
					<!-- Start Single Wedget -->
					<div class="col-lg-2 col-md-6 col-sm-6 col-12 md-mt-40 sm-mt-40">
						<div class="footer__widget">
							<h4>Categories</h4>
							<div class="footer__innner">
								<div class="ftr__latest__post">
									<ul class="ftr__catrgory">
										<li><a href="#">Painting</a></li>
										<li><a href="#">Alphabet Matching</a></li>
										<li><a href="#">Drawing</a></li>
										<li><a href="#">Swimming</a></li>
										<li><a href="#">Sports & Games</a></li>
										<li><a href="#">Painting</a></li>
										<li><a href="#">Alphabet Matching</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Wedget -->
					<!-- Start Single Widget -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 md-mt-40 sm-mt-40">
						<div class="footer__widget">
							<h4>Twitter Widget</h4>
							<div class="footer__innner">
								<div class="dcare__twit__wrap">
									<!-- Start Single -->
									<div class="dcare__twit d-flex">
										<div class="dcare__twit__icon">
											<i class="fa fa-twitter"></i>
										</div>
										<div class="dcare__twit__details">
											<p>Lorem ipsum dolor sit  consect ietur adipisicing sed  eiipsa<a href="#"># twitter .com?web/lnk/statement</a></p>
											<span><i class="fa fa-clock-o"></i>30th Dec, 2017</span>
											<span><i class="fa fa-calendar"></i>30th Dec, 2017</span>
										</div>
									</div>
									<!-- End Single -->
									<!-- Start Single -->
									<div class="dcare__twit d-flex">
										<div class="dcare__twit__icon">
											<i class="fa fa-twitter"></i>
										</div>
										<div class="dcare__twit__details">
											<p>Lorem ipsum dolor sit  consect ietur adipisicing sed  eiipsa<a href="#"># twitter .com?web/lnk/statement</a></p>
											<span><i class="fa fa-clock-o"></i>30th Dec, 2017</span>
											<span><i class="fa fa-calendar"></i>30th Dec, 2017</span>
										</div>
									</div>
									<!-- End Single -->
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Widget -->
				</div>
			</div>
		</div>
		<!-- Start Copyright Area -->
		<div class="copyright bg__cat--9">
			<div class="container">
				<div class="row align-items-center copyright__wrapper justify-content-center">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="coppy__right__inner text-center">
							<p><i class="fa fa-copyright"></i>All Right Reserved.<a href="#" target="_blank"> Devitems</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Copyright Area -->
	</footer>
	<!-- Start Footer Area -->
</div>