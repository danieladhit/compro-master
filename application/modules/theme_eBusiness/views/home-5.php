<!-- Strat Slider Area -->
<div class="slide__carosel owl-carousel owl-theme">
	<div class="slider__area slider--five bg-pngimage--5 d-flex slider__fixed--height justify-content-end align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-6 offset-md-4 col-md-8 col-sm-12">
					<div class="slider__activation">
						<!-- Start Single Slide -->
						<div class="slide">
							<div class="slide__inner">
								<h6>“new for kids”</h6>
								<h1>2018 New Arrival For New Way</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisic ming elit, sed do ei Excepteur.Tnam Bajki      vntoccaecat cupida proident, sunt in culpa qui dese runt mol .</p>
								<div class="slider__btn">
									<a class="dcare__btn btn__org hover--theme" href="#">Read More</a>
								</div>
							</div>
						</div>
						<!-- End Single Slide -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="slider__area slider--five bg-pngimage--5 d-flex slider__fixed--height justify-content-end align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-6 offset-md-4 col-md-8 col-sm-12">
					<div class="slider__activation">
						<!-- Start Single Slide -->
						<div class="slide">
							<div class="slide__inner">
								<h6>“new for kids”</h6>
								<h1>2018 New Arrival For New Way</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipisic ming elit, sed do ei Excepteur.Tnam Bajki      vntoccaecat cupida proident, sunt in culpa qui dese runt mol .</p>
								<div class="slider__btn">
									<a class="dcare__btn btn__org hover--theme" href="#">Read More</a>
								</div>
							</div>
						</div>
						<!-- End Single Slide -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Slider Area -->
<!-- Start Product Area -->
<section class="dcare__product bg--white section-padding--lg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line title__line--4">New Arrival Product</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="product__nav nav justify-content-center" role="tablist">
					<a class="nav-item nav-link active" data-toggle="tab" href="#nav-all" role="tab">Children Cloth</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-shoe" role="tab">Children Shoes</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-food" role="tab">Baby food</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-toys" role="tab">Toys</a>
				</div>
			</div>
		</div>
		<div class="product__contant">
			<div class="row single__product__item tab-pane fade show active" id="nav-all" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Off</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-shoe" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Off</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-food" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Off</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-toys" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/1.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/2.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Off</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/3.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-3 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/dress/4.png" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<h4><a href="shop-single.html">White T-shirt</a></h4>
							<span>$70</span>
							<div class="pro__btn">
								<a class="dcare__btn btn__f1f1f1" href="shop-single.html">Add To Cart</a>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
		</div>
	</div>
</section>
<!-- End Product Area -->
<!-- Start New Arrival Product -->
<section class="new__arrival__product bg--white">
	<div class="container-fluid">
		<div class="row align-items-center flex-wrap bg__cat--2">
			<div class="col-md-12 col-sm-12 col-12 col-lg-5 bg--white">
				<div class="dacre__arrival__product">
					<div class="arrival__product__activation owl-carousel owl-theme clearfix">
						<div class="arrival__thumb">
							<img src="<?=$current_theme ?>images/product/big-pro/1.jpg" alt="arrival product">
							<div class="new__arri__action">
								<span>New</span>
							</div>
						</div>
						<div class="arrival__thumb">
							<img src="<?=$current_theme ?>images/product/big-pro/1.jpg" alt="arrival product">
							<div class="new__arri__action">
								<span>Hot</span>
							</div>
						</div>
						<div class="arrival__thumb">
							<img src="<?=$current_theme ?>images/product/big-pro/1.jpg" alt="arrival product">
							<div class="new__arri__action">
								<span>Hot</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-12 col-lg-7 md-mt-40 sm-mt-40">
				<div class="new__arrival__inner">
					<div class="new__arival__content">
						<h2>New Arrival</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
						<span>New Offer is Going on</span>
						<h6>Denim Back Pack good style hunting</h6>
						<p>Lorem ipsum dolor sit cnr adipi sicing elit, sed do eiudolor sit amet, conse ctetuir adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magaliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laaliquix commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
						<div class="new__arrival__btn">
							<a class="dcare__btn btn__org hover--theme" href="cart.html">Shop Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End New Arrival Product -->
<!-- Start Banner Area -->
<section class="dcare__banner__area section-padding--lg bg--white">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
				<div class="bcare__banner">
					<div class="banner__thumb">
						<img src="<?=$current_theme ?>images/banner/mid-img/1.jpg" alt="banner images">
					</div>
					<div class="banner__inner banner__content--bottom">
						<h4>“Cool SweatShirt”</h4>
						<h2>50% Sale Up to all</h2>
					</div>
					<div class="banner__offer">
						<span>On Sale</span>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12 sm-mt-40">
				<div class="bcare__banner__warpper">
					<!-- Start Single Banner -->
					<div class="bcare__banner">
						<div class="banner__thumb">
							<img src="<?=$current_theme ?>images/banner/mid-img/2.jpg" alt="banner images">
						</div>
						<div class="banner__inner banner__content--right">
							<h4>Adorable</h4>
							<h2>Floral Sweater</h2>
						</div>
					</div>
					<!-- End Single Banner -->
					<!-- Start Single Banner -->
					<div class="bcare__banner mt--30 sm-mt-40">
						<div class="banner__thumb">
							<img src="<?=$current_theme ?>images/banner/mid-img/3.jpg" alt="banner images">
						</div>
						<div class="banner__inner">
							<h4>New Launch</h4>
							<h2>Mini Skirt</h2>
						</div>
					</div>
					<!-- End Single Banner -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Banner Area -->
<!-- Start Product Area -->
<section class="dcare__product bg--white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line title__line--4">All Product</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="product__nav nav justify-content-center" role="tablist">
					<a class="nav-item nav-link active" data-toggle="tab" href="#nav-dress" role="tab">Dress</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-acce" role="tab">Accessories</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-games" role="tab">Games</a>
					<a class="nav-item nav-link" data-toggle="tab" href="#nav-foob" role="tab">Baby food</a>
				</div>
			</div>
		</div>
		<div class="all-product product__contant">
			<div class="row single__product__item tab-pane fade show active" id="nav-dress" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/6.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">T-shirt</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/1.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Leather Shoes</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/2.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/4.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/5.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-acce" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/1.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">T-shirt</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/2.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Leather Shoes</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/4.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/5.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-games" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/6.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">T-shirt</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/2.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Leather Shoes</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/4.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/5.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
			<div class="row single__product__item tab-pane fade" id="nav-foob" role="tabpanel">
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/6.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">T-shirt</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/1.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>New</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Leather Shoes</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/2.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/4.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Hot</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Punch Clip</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/5.jpg" alt="product images">
							</a>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
				<!-- Start Single Product -->
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<!-- Start Product -->
					<div class="product--2">
						<div class="product__imges">
							<a href="shop-single.html">
								<img src="<?=$current_theme ?>images/product/mid-img/3.jpg" alt="product images">
							</a>
							<div class="pro__ofer">
								<span>Sale</span>
							</div>
							<div class="product__cart__wrapper">
								<ul class="cart__list">
									<li><a href="cart.html"><span class="ti-shopping-cart"></span></a></li>
									<li><a data-toggle="modal" data-target="#productModal" title="Quick View" class="quick-view modal-view detail-link" href="#"><span class="ti-search"></span></a></li>
									<li><a href="wishlist.html"><span class="ti-heart"></span></a></li>
								</ul>
							</div>
						</div>
						<div class="product__inner">
							<div class="pro__title">
								<h4><a href="shop-single.html">Baby Dress</a></h4>
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="pro__prize">
								<span class="old__prize">$65.00</span>
								<span>$40.00 </span>
							</div>
						</div>
					</div>
					<!-- End Product -->
				</div>
				<!-- End Single Product -->
			</div>
		</div>
	</div>
</section>		
<!-- End Product Area -->
<!-- Start Blog Area -->
<section class="dcare__blog__area section-padding--lg bg--white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="section__title text-center">
					<h2 class="title__line title__line--4">Our Latest Blog</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>
				</div>
			</div>
		</div>
		<div class="row mt--40">
			<!-- Start Single Blog -->
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="blog__2">
					<div class="blog__thumb">
						<a href="blog-details.html">
							<img src="<?=$current_theme ?>images/blog/bl-2/1.jpg" alt="blog images">
						</a>
					</div>
					<div class="blog__inner">
						<div class="blog__hover__inner">
							<h2><a href="blog-details.html">Don’t have basic knowledge..! So, stay learning with us</a></h2>
							<div class="bl__meta">
								<p>Children Blog : Post By Ariana / 10th November, 2017</p>
							</div>
							<div class="bl__details">
								<p>Lorem ipsum dolor sit amet, consect adsicinge elit, sed do eiusmod tempor incidi.</p>
							</div>
							<div class="blog__btn">
								<a class="bl__btn" href="blog-details.html">Read More</a>
								<a class="bl__share__btn" href="#"><i class="fa fa-share-alt"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Blog -->
			<!-- Start Single Blog -->
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="blog__2">
					<div class="blog__thumb">
						<a href="blog-details.html">
							<img src="<?=$current_theme ?>images/blog/bl-2/2.jpg" alt="blog images">
						</a>
					</div>
					<div class="blog__inner">
						<div class="blog__hover__inner">
							<h2><a href="blog-details.html">Don’t have basic knowledge..! So, stay learning with us</a></h2>
							<div class="bl__meta">
								<p>Children Blog : Post By Ariana / 10th November, 2017</p>
							</div>
							<div class="bl__details">
								<p>Lorem ipsum dolor sit amet, consect adsicinge elit, sed do eiusmod tempor incidi.</p>
							</div>
							<div class="blog__btn">
								<a class="bl__btn" href="blog-details.html">Read More</a>
								<a class="bl__share__btn" href="#"><i class="fa fa-share-alt"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Blog -->
			<!-- Start Single Blog -->
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="blog__2">
					<div class="blog__thumb">
						<a href="blog-details.html">
							<img src="<?=$current_theme ?>images/blog/bl-2/3.jpg" alt="blog images">
						</a>
					</div>
					<div class="blog__inner">
						<div class="blog__hover__inner">
							<h2><a href="blog-details.html">Don’t have basic knowledge..! So, stay learning with us</a></h2>
							<div class="bl__meta">
								<p>Children Blog : Post By Ariana / 10th November, 2017</p>
							</div>
							<div class="bl__details">
								<p>Lorem ipsum dolor sit amet, consect adsicinge elit, sed do eiusmod tempor incidi.</p>
							</div>
							<div class="blog__btn">
								<a class="bl__btn" href="blog-details.html">Read More</a>
								<a class="bl__share__btn" href="#"><i class="fa fa-share-alt"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Single Blog -->
		</div>
	</div>
</section>
<!-- End Blog Area -->
<!-- Start Subscribe Area -->
<section class="dcare__testimonial__area subscribe__style--5">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="subscribe__inner">
					<h2>Subscribe To Our Newsletter</h2>
					<div class="newsletter__form">
						<div class="input__box">
							<div id="mc_embed_signup">
								<form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
									<div id="mc_embed_signup_scroll" class="htc__news__inner">
										<div class="news__input">
											<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Enter Your E-mail" required>
										</div>
										<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
										<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
										<div class="clearfix subscribe__btn"><input class="bst__btn btn--white__color" type="submit" value="Send Now" name="subscribe" id="mc-embedded-subscribe">
										</div>
									</div>
								</form>
							</div>
						</div>        
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="testimonial--4">
					<div class="testimonial__slide--4 owl-carousel owl-theme clearfix">
						<div class="testimonial__inner">
							<div class="clint__img">
								<img src="<?=$current_theme ?>images/testimonial/clint-2/1.png" alt="clint images">
							</div>
							<div class="clint__info">
								<h6>Lora alica</h6>
								<span>Gardients of student</span>
								<p>Lorem ipsum dolor t dolore magna aliqua veniam, mollit anim id est lsumih</p>
							</div>
						</div>
						<div class="testimonial__inner">
							<div class="clint__img">
								<img src="<?=$current_theme ?>images/testimonial/clint-2/1.png" alt="clint images">
							</div>
							<div class="clint__info">
								<h6>Lora alica</h6>
								<span>Gardients of student</span>
								<p>Lorem ipsum dolor t dolore magna aliqua veniam, mollit anim id est lsumih</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Subscribe Area -->
<!-- Start Brand Area -->
<div class="dcare__brand__area bg--white section-padding--lg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<ul class="brand__list d-flex flex-wrap flex-lg-nowrap justify-content-between">
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/1.png" alt="brand images"></a></li>
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/2.png" alt="brand images"></a></li>
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/3.png" alt="brand images"></a></li>
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/4.png" alt="brand images"></a></li>
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/2.png" alt="brand images"></a></li>
					<li><a href="#"><img src="<?=$current_theme ?>images/brand/1.png" alt="brand images"></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
		<!-- End Brand Area -->