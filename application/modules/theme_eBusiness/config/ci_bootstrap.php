<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

	// Site name
	'site_name' => 'eBusiness Theme',

	// Current Theme
	'current_theme' => '/compro-master/themes/eBusiness/',

	// google api key
	'google_api_key' => 'AIzaSyAshH-WsVH1fx2b0nNdnqH-iSpWu66N9N4',

	// Default page title prefix
	'page_title_prefix' => '',

	// Default page title
	'page_title' => '',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			'themes/junior-home/js/vendor/modernizr-3.5.0.min.js',
		),
		'foot'	=> array(
			"themes/eBusiness/lib/jquery/jquery.min.js",
			"themes/eBusiness/lib/bootstrap/js/bootstrap.min.js",
			"themes/eBusiness/lib/owlcarousel/owl.carousel.min.js",
			"themes/eBusiness/lib/venobox/venobox.min.js",
			"themes/eBusiness/lib/knob/jquery.knob.js",
			"themes/eBusiness/lib/wow/wow.min.js",
			"themes/eBusiness/lib/parallax/parallax.js",
			"themes/eBusiness/lib/easing/easing.min.js",
			"themes/eBusiness/lib/nivo-slider/js/jquery.nivo.slider.js",
			"themes/eBusiness/lib/appear/jquery.appear.js",
			"themes/eBusiness/lib/isotope/isotope.pkgd.min.js",
			"themes/eBusiness/contactform/contactform.js",
			"themes/eBusiness/js/main.js",
		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900',
			'themes/eBusiness/lib/bootstrap/css/bootstrap.min.css',
			'themes/eBusiness/lib/nivo-slider/css/nivo-slider.css',
			'themes/eBusiness/lib/owlcarousel/owl.carousel.css',
			'themes/eBusiness/lib/owlcarousel/owl.transitions.css',
			'themes/eBusiness/lib/font-awesome/css/font-awesome.min.css',
			'themes/eBusiness/lib/animate/animate.min.css',
			'themes/eBusiness/lib/venobox/venobox.css',
			'themes/eBusiness/css/nivo-slider-theme.css',
			'themes/eBusiness/css/style.css',
			'themes/eBusiness/css/responsive.css',
		)
	),

	// Default CSS class for <body> tag
	'body_class' => '',
	
	// Multilingual settings
	'languages' => array(
		/*'default'		=> 'en',
		'autoload'		=> array('general'),
		'available'		=> array(
			'en' => array(
				'label'	=> 'English',
				'value'	=> 'english'
			),
			'zh' => array(
				'label'	=> '繁體中文',
				'value'	=> 'traditional-chinese'
			),
			'cn' => array(
				'label'	=> '简体中文',
				'value'	=> 'simplified-chinese'
			),
			'es' => array(
				'label'	=> 'Español',
				'value' => 'spanish'
			)
		)*/
	),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'Home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => '',

	// Restricted pages
	'page_auth' => array(
	),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',
		
		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'eBusiness-theme';