  <!-- BANNER -->
 <!--  <div class="bannercontainer bannerV3">
   <div class="fullscreenbanner-container">
     <div class="container">
 
     </div>
 
     <div class="owl-carousel banner-slider carousel-inner">
       <div class="slide">
         <a href="<?//=base_url() ?>our-school/gp-kids"><img src="<?//=image('src/images/slide-kindergarten.jpg', 'extra_large') ?>" class="img-responsive" alt=""></a>
       </div>
       <div class="slide">
         <a href="<?//=base_url() ?>our-school/gp-montessori"><img src="<?//=image('src/images/montess.JPG', 'extra_large') ?>" class="img-responsive" alt=""></a>
       </div>
       <div class="slide">
         <a href="<?//=base_url() ?>our-school/gp-elementary"><img src="<?//=image('src/images/sd.png', 'extra_large') ?>" class="img-responsive" alt=""></a>
       </div>
       <div class="slide">
         <a href="<?//=base_url() ?>our-school/gp-jhs"><img src="<?//=image('src/images/smp.JPG', 'extra_large') ?>" class="img-responsive" alt=""></a>
       </div>
       <div class="slide">
         <a href="<?//=base_url() ?>our-school/gp-shs"><img src="<?//=image('src/images/slide-sma.jpg', 'extra_large') ?>" class="img-responsive" alt=""></a>
       </div>
     </div>
 
   </div>
 </div> -->

 <!-- BANNER -->
 <div class="bannercontainer bannerV2">
  <div class="fullscreenbanner-container">
    <div class="fullscreenbanner">
      <ul>
        <li data-transition="slidehorizontal" data-slotamount="3" data-masterspeed="700" data-title="Slide 1">
          <img src="<?=image('src/images/slide-kindergarten.jpg', 'extra_large') ?>" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
          <div class="slider-caption container">
            <div class="tp-caption rs-caption-1 sft text-center" style="color: #0000B9" 
            data-x="center"
            data-y="210"
            data-speed="1000"
            data-start="500"
            data-easing="Back.easeInOut"
            data-endspeed="300">
            Welcome to Global Prestasi School
          </div>

          <div class="tp-caption rs-caption-2 sft text-center"
          data-x="center"
          data-y="320"
          data-speed="1000"
          data-start="1500"
          data-easing="Power4.easeOut"
          data-endspeed="300"
          data-endeasing="Power1.easeIn"
          data-captionhidden="off">
         <!--  The Only One Stop School in Bekasi -->
        </div>
        <div class="tp-caption rs-caption-3 sft text-center"
        data-x="center"
        data-y="400"
        data-speed="800"
        data-start="2000"
        data-easing="Power4.easeOut"
        data-endspeed="300"
        data-endeasing="Power1.easeIn"
        data-captionhidden="off">
       <a href="<?=base_url() ?>about-gps/history" class="btn primary-btn">About Us</a>
      </div>
    </div>
  </li>
  <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
    <img src="<?=image('src/images/slide-sma.jpg', 'extra_large') ?>" alt="slidebg2" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
    <div class="slider-caption container">
      <div class="tp-caption rs-caption-1 sft"
      data-x="0"
      data-hoffset="0"
      data-y="140"
      data-speed="800"
      data-start="500"
      data-easing="Back.easeInOut"
      data-endspeed="300">
      Vision 
    </div>

    <div class="tp-caption rs-caption-2 sft"
    data-x="0"
    data-hoffset="0"
    data-y="250"
    data-speed="1000"
    data-start="1500"
    data-easing="Power4.easeOut"
    data-endspeed="300"
    data-endeasing="Power1.easeIn"
    data-captionhidden="off">
    To be a learning community which has a faith, global minded, high achievement and<br>useful for families and our nation.
  </div>
  <div class="tp-caption rs-caption-3 sft"
  data-x="0"
  data-hoffset="0"
  data-y="400"
  data-speed="800"
  data-start="2000"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off">
 <!--  <a href="#" class="btn primary-btn">Start Now</a> -->
</div>
</div>
</li>
<li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="700"  data-title="Slide 3">
  <img src="<?=image('src/images/slide-smp.jpg', 'extra_large') ?>" alt="slidebg3" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
  <div class="slider-caption container">
    <div class="tp-caption rs-caption-1 sft text-right"
    data-hoffset="-50"
    data-x="right"
    data-y="210"
    data-speed="800"
    data-start="500"
    data-easing="Back.easeInOut"
    data-endspeed="300">
    Join Global Prestasi School
  </div>

  <div class="tp-caption rs-caption-2 sft text-right"
  data-hoffset="-50"
  data-x="right"
  data-y="350"
  data-speed="1000"
  data-start="1500"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off">
  You can learn more about our school in a short and informative information session.
</div>
<div class="tp-caption rs-caption-3 sft text-right"
data-hoffset="-50"
data-x="right"
data-y="400"
data-speed="800"
data-start="2000"
data-easing="Power4.easeOut"
data-endspeed="300"
data-endeasing="Power1.easeIn"
data-captionhidden="off">
<a href="<?=base_url('information/admission') ?>" class="btn primary-btn">Get Admission</a>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>

<!-- BANNER BOTTOM-->
<div class="bannercontainer-bottom">
  <div class="fullscreenbanner-container">
    <div class="container">
      <div class="owl-carousel bannerBottom-slider">
        <div class="slide">
          <div class="slide-info text-center">
            <h2 class="title">GPS Philosophy</h2>
            <p>"We believe that every individual has potential and uniqueness to develop into holistic individual with a strong character, academic foundation, and skills".</p>
          </div>
        </div>
        <div class="slide">
          <div class="slide-info text-center">
            <h2 class="title">Vision</h2>
            <p>To be a learning community which has a faith, global minded, high achievement and useful for families and our nation.</p>
          </div>
        </div>
        <div class="slide">
          <div class="slide-info text-center">
            <h2 class="title">Mission</h2>
            <p>To develop diverse school members through aligned quality learning, good character building and empowering global education.</p>
          </div>
        </div>
      </div>
      <i class="fa fa-bullhorn" aria-hidden="true"></i>
    </div>

  </div>
</div>

<!-- popup -->

<!-- <div style='display:none'><a id="upcomingEventPopup" href="<?//=base_url('upcoming-event/'.$upcoming_event->blog_slug) ?>" title=''><img style="width: 100%;height: auto;" class="img-responsive" src="<?//=upload_url('blog/'.$upcoming_event->blog_picture) ?>" alt="Image"></a></div> -->

<div class="container" style="padding-bottom: 40px;">

  <!--NEWS SECTION -->
  <div class="" style="padding-top: 20px;">
    <div class="row">
      <div class="col-sm-7 col-xs-12">
        <div class="upcoming_events event-col">
          <div class="related_post_sec single_post" style="margin-bottom: 0px">
            <div class="sectionTitle title-block">
              <h3>Upcoming event</h3>
            </div>
            <a class="more" href="<?=base_url('upcoming-event') ?>">View All ></a>
            <!-- <div class="owl-carousel upcoming-event-slider"> -->
              <div class="slide">
                <div class="rel_right" style="padding-left: 0px">
                  <h4><a href="<?=base_url('upcoming-event/'.$upcoming_event->blog_slug) ?>"><?=$upcoming_event->blog_title ?></a></h4>
                  <span class="rel_thumb" style="width: 100%;">
                    <a href="<?=base_url('upcoming-event/'.$upcoming_event->blog_slug) ?>" title=''><img src="<?=upload_url('blog/'.$upcoming_event->blog_picture) ?>" alt="Image"></a>
                  </span>
                </div>
              </div>
              <!--  </div>   -->
            </div>
          </div>
        </div>
        <div class="col-sm-5 col-xs-12">
          <div class="related_post_sec single_post">
            <div class="sectionTitle title-block">
              <h3>Recent News</h3>
            </div>
            <a class="more" href="<?=base_url('blog') ?>">View All &gt;</a>
            <ul>
              <?php foreach($event as $key => $row){ ?>
              <li>
                <span class="rel_thumb">
                  <a href="<?=base_url('blog/'.$row->blog_slug) ?>"><img src="<?=upload_url('blog/'.$row->blog_picture) ?>" alt=""></a>
                </span><!--end rel_thumb-->
                <div class="rel_right">
                  <h4><a href="<?=base_url('blog/'.$row->blog_slug) ?>"><?=$row->blog_title ?></a></h4>
                  <div class="meta">
                    <span class="author">Posted by: <a href="#"><?=$row->username ?></a></span>
                    <!--<span class="date">on: <a href="#">
                        <?//=date('d F Y', strtotime($row->create_date)) ?>
                        <?php //echo $row->blog_date; ?>-->
                        </a></span>
                  </div>
                  <p><?=character_limiter($row->blog_description, 150) ?></p>
                </div><!--end rel right-->
              </li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- COURSE CATEGORY -->
  <section class="courseCategory padding paralax clearfix" style="background-image: url(src/images/paralax03a.jpg);">
    <div class="container">
      <div class="sectionTitle text-center">
        <h3>EXCELLENCES PROGRAM</h3>
      </div>

      <div class="row">
        <div class="col-sm-3 col-xs-6">
          <div class="text-box">
            <div class="text-box-icon">
              <!-- <img src="src/images/i-icon.png" alt=""> -->
            </div>
            <div class="text-box-top">
              <h4><img style="width: 35px; height: auto;" src="src/images/i-icon.png" alt=""> <a href="">nformation Technology</a></h4>
              <p>ICT- based learning</p>
            </div>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="text-box">
            <div class="text-box-icon">
              <!-- <img src="src/images/d-icon.png" alt=""> -->
            </div>
            <div class="text-box-top">
              <h4><img style="width: 35px; height: auto;" src="src/images/d-icon.png" alt=""> <a href="">iversity</a></h4>
              <p>Harmony in diversity</p>
            </div>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="text-box">
            <div class="text-box-icon">
              <!-- <img src="src/images/e-icon.png" alt=""> -->
            </div>
            <div class="text-box-top">
              <h4><img style="width: 35px; height: auto;" src="src/images/e-icon.png" alt=""> <a href="">nglish</a></h4>
              <p>English environment</p>
            </div>
          </div>
        </div>

        <div class="col-sm-3 col-xs-6">
          <div class="text-box">
            <div class="text-box-icon">
              <!-- <img src="src/images/a-icon.png" alt=""> -->
            </div>
            <div class="text-box-top">
              <h4><img style="width: 35px; height: auto;" src="src/images/a-icon.png" alt=""> <a href="">chievement</a></h4>
              <p>Academic, non academic national and international level</p>
            </div>
          </div>
        </div>
      </div>

      <div class="btnArea text-center">
        <a href="<?=base_url() ?>about-gps/history" class="btn btn-default commonBtn">View more</a>
      </div>
    </div>
  </section>-->

  <div class="clearfix"></div>

  <!-- LIGHT SECTION -->
  <div class="clearfix padding feature-section1">
    <div class="sectionTitle text-center">
      <h3>Core Values</h3>
    </div>

    <div class="row">
      <div class="col-md-2 col-sm-4 col-md-offset-1 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-heart" aria-hidden="true"></i></span>
          <h3>Care</h3>

        </div>
      </div>

      <div class="col-md-2 col-sm-4 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-lightbulb-o" aria-hidden="true"></i></span>
          <h3>Competency</h3>
        </div>
      </div>

      <div class="col-md-2 col-sm-4 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-handshake-o" aria-hidden="true"></i></span>
          <h3>Commitment</h3>
        </div>
      </div>

      <div class="col-md-2 col-md-offset-0 col-sm-4 col-sm-offset-2 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-gears" aria-hidden="true"></i></span>
          <h3>Core Teamwork</h3>
        </div>
      </div>

      <div class="col-md-2 col-sm-4 col-xs-6 text-center">
        <div class="feature-box v3">
          <span><i class="fa fa-key" aria-hidden="true"></i></span>
          <h3>Consistency</h3>
        </div>
      </div>

    </div>
  </div>

  <!-- paralax 1 -->
  <div class="padding clearfix paralax" style="background-image: url(src/images/paralax01.jpg);">
    <div class="container">
      <div class="paralax-text text-center paralaxInner">
        <h2>Join Global Prestasi School</h2>
        <p>You can learn more about our school in a short and informative information session.  We are proud of ourselves on being more than facilities and curriculum so please spend a moment to talk about child’s future education.  Your questions are very welcome and look forward to talking to you more.</p>
        <a href="<?=base_url('information/admission') ?>" class="btn btn-default commonBtn">Get Admission</a>
      </div><!-- row -->
    </div>
  </div>

  <!-- VIDEO SECTION -->
  <div class="video-section padding">
    <div class="container">
      <div class="sectionTitle title-block">
        <h3>Events</h3>
      </div>
      <a class="more" href="<?=base_url('videos') ?>">View All &gt;</a>
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="videoLeft">
            <div class="related_post_sec single_post">
              <span class="rel_thumb">
                <a class="popup-youtube" href="src/videos/Welcome.mp4" >
                  <img src="src/videos/Welcome.png"></a>
                  <div class="rel_right">
                    <h4><a href="single-post-right-sidebar.html">Welcome to Global Prestasi School</a></h4>
                    <div class="meta">
                      <span class="author">Posted by: <a href="#">Admin </a></span>
                      <span class="date">on: <a href="#">January 24, 2016</a></span>
                    </div>
                  </div>
                </span></div>
              </div>
            </div>

            <div class="col-md-6 col-xs-12">
              <div class="videoRight">
                <div class="related_post_sec single_post">
                  <ul>
                    <?php foreach($video as $key => $row){ ?>
                    <li>
                      <span class="rel_thumb">
                        <a class="popup-youtube" href="<?=$row->video_link ?>" savefrom_lm_index="2">
                          <img src="http://img.youtube.com/vi/<?=youtube_id($row->video_link) ?>/hqdefault.jpg">
                        </a>
                      </span>
                      <div class="rel_right">
                        <h4><a href="single-post-right-sidebar.html"><?=$row->video_title ?></a></h4>
                        <div class="meta">
                          <span class="author">Posted by: <a href="#">Admin </a></span>
                          <span class="date">on: <a href="#">January 24, 2016</a></span>
                        </div>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--  paralax 2 (connect with us) -->
      <div class="padding clearfix paralax" style="background-image: url(src/images/paralax02.jpg);">
        <div class="container">
          <div class="paralax-text text-center paralaxInner">
            <h2>Connect with us</h2>
            <p>Simply Follow, Like or Connect with us</p>
            <ul class="list-inline">
              <li><a href="https://twitter.com/globalprestasi?lang=en"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/globalprestasi/"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://plus.google.com/104649733451475032771"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.instagram.com/explore/locations/8885898/global-prestasi-school/"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div><!-- row -->
        </div>
      </div>


      <div class="clearfix padding gallery-section">
        <div class="container">
          <div class="sectionTitle title-block">
            <h3>Gallery</h3>
          </div>
          <a class="more" href="<?=base_url('images') ?>">View All &gt;</a>
          <div class="custom photo_gallery">
            <ul class="gallery">
              <?php foreach($galery_street_view as $key => $row){ ?>
              <li>
                <a class="popup-gmaps" href="<?=get_iframe_src($row->galeri_description) ?>" title="<?=$row->galeri_title ?>">
                  <img src="<?=streetview_image_url($row->galeri_link) ?>" alt=""> 
                  <img src="uploads/galeri/<?=$row->galeri_slug ?>/<?=$row->galeri_picture ?>" alt="">
                  <div class="overlay">
                    <span class="zoom">
                      <i class="fa fa-search"></i>
                    </span>
                  </div>
                </a>
              </li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>


       <!--  <div class="testimonial-section clearfix">
         <div class="container">
           <div class="row">
             <div class="col-xs-12 col-sm-6">
               <div class="testimonial">
                 <div class="carousal_content">
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
                 </div>
                 <div class="carousal_bottom">
                   <div class="thumb">
                     <img src="src/images/SARA-LISBON_Art-Student.jpg" alt="" draggable="false">
                   </div>
                   <div class="thumb_title">
                     <span class="author_name">Sara Lisbon</span>
                     <span class="author_designation">Student<a href="#"> English Literature</a></span>
                   </div>
                 </div>
               </div>testimonial
             </div>col-xs-12
             <div class="col-xs-12 col-sm-6">
               <div class="features">
                 <h3>GPS Overview</h3>
                 <p>Global Prestasi School (GPS) consist of Montessori Pre-school, Elementary School (ES), Junior High School (JHS), and Senior High School (SHS), which are managed and operated by Yayasan Harapan Global Mandiri.</p>
                 <p>Global Prestasi JHS & SHS were founded in 2005 while Global Prestasi ES was established in 2007. Global Prestasi Montessori was launched in 2016. </p>
                 <p>GPS has been awarded an accreditation grade of "A" since 2007 and is officialy recognized as "Model School" by Dinas Pendidikan Bekasi</p>
               </div>
             </div>col-xs-12
           </div>row
         </div>container
       </div>
     -->
