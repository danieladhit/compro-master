<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('calendar_model', 'calendar');
		$this->load->model('global_model', 'global');

		// get user group
		$this->user = $this->ion_auth->get_users_groups()->row();

		// get user session
		$this->user_session = $this->ion_auth->user()->row();
	}

	public function index()
	{
		$crud = $this->generate_crud('calendar_events');
		$state = $crud->getState();
		$crud->columns('admin_id', 'room_id', 'start', 'title', 'equipment', 'description');
		$crud->set_relation('admin_id', 'admin_users', '{first_name} {last_name}');
		$crud->set_relation('room_id', 'calendar_rooms', 'name');

		// condition 
		$crud->order_by('start','asc');
		$crud->limit(20);

		// callback
		$crud->callback_column('equipment', array($this, 'callback_equipment'));
		
		// disable direct create / delete Admin User
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();


		$this->mPageTitle = 'Upcoming Events';
		$this->render_crud();
	}

	public function callback_equipment($value, $row)
	{
		return str_replace("#eq#", ",", $value);
	}

}
