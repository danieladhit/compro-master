<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {

	public function index()
	{
		$this->nav = 'home';
		$this->nav_child = 'home';
		
		$this->load->model('global_model', 'global');

		$upcoming_category = $this->global->get_data('category', array('category_slug' => 'upcoming-event'), TRUE);

		$this->global->join_user_admin('blog');
		$this->db->order_by('blog_id', 'DESC');
		$this->mViewData['upcoming_event'] = $this->global->get_data('blog', array('category_id' => $upcoming_category->category_id), TRUE);


		$event_category = $this->global->get_data('category', array('category_slug' => 'event'), TRUE);

		$this->db->limit(2);
		$this->global->join_user_admin('blog');
		$this->db->order_by('blog_date', 'DESC');
		$this->mViewData['event'] = $this->global->get_data('blog', array('category_id' => $event_category->category_id));


		$this->db->limit(4);
		$this->db->order_by('create_date', 'DESC');
		//	$this->db->order_by('blog_date', 'DESC');
		//$this->global->join_user_admin('video');
		$video_list = $this->global->get_data('video', array('video_type' => 'in'));
		//$first_video = array_shift($video_list);

		//$this->mViewData['first_video'] = $first_video;

		$this->mViewData['video'] = $video_list;


		$galery_street_view_category = $this->global->get_data('galeri_category', array('galeri_category_slug' => 'google-street-view'), TRUE);

		$this->db->limit(4);
		$this->db->order_by('create_date', 'DESC');
		//$this->global->join_user_admin('galeri');
		$this->mViewData['galery_street_view'] = $this->global->get_data('galeri', array('galeri_category_id' => $galery_street_view_category->galeri_category_id));

		
		$this->render_with_db('home');
		
	}

	public function second() {
		$this->render('home-2');
	}

	public function third() {
		$this->render('home-3');
	}

	public function fourth() {
		$this->render('home-4');
	}

	public function fifth() {
		$this->render('home-5');
	}
	
}
