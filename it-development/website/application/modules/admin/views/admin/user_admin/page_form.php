<div id="content-wrapper">
        <div class="page-header">
            <h1><span class="text-light-gray"><a href="<?php echo base_url()?>admin/user_admin"><?php echo ucwords('user') ?></a> / </span><?php echo ucwords($heading)?></h1>
        </div> <!-- / .page-header -->
<!-- 5. $SUMMERNOTE_WYSIWYG_EDITOR =================================================================
        Summernote WYSIWYG-editor
-->
        <!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js)-->
        <?php $this->load->view('admin/page_form_script'); ?>
        <!-- Javascript -->
        <script>
            init.push(function () {
            });
        </script>
        <!-- / Javascript -->
        <?php $this->load->view('admin/field/flash_message'); ?>
        <form action="<?php echo $form_action?>" method="post" enctype="multipart/form-data" class="panel form-horizontal form-bordered">
                    <div class="panel-heading">
                        <span class="panel-title"><?php echo set_value('user_name', isset($default['user_name']) ? ucwords($default['user_name']) : ucwords($heading)); ?></span>
                    </div>
                    <div class="panel-body no-padding-hr">
                        <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t <?php if(form_error('user_name') != '') { echo 'has-error'; } ?>">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama user</label>
                                <div class="col-sm-8">
                                    <input type="text" required name="user_name" class="form-control" placeholder="Nama" value="<?php echo set_value('user_name', isset($default['user_name']) ? $default['user_name'] : ''); ?>">
                                    <input type="hidden" name="user_id" class="form-control" placeholder="id user" value="<?php echo set_value('user_id', isset($default['user_id']) ? $default['user_id'] : ''); ?>">
                                    <?php echo form_error('user_name', '<span class="help-block"><i class="fa fa-warning"></i> ', '</span>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t <?php if(form_error('username') != '') { echo 'has-error'; } ?>">
                            <div class="row">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-8">
                                    <input type="text" required name="username" class="form-control" placeholder="Nama" value="<?php echo set_value('username', isset($default['username']) ? $default['username'] : ''); ?>">
                                    <?php echo form_error('username', '<span class="help-block"><i class="fa fa-warning"></i> ', '</span>'); ?>
                                </div>
                            </div>
                        </div>
						<div class="form-group no-margin-hr panel-padding-h no-padding-t no-border-t <?php if(form_error('userfile') != '') { echo 'has-error'; } ?>">
                            <div class="row">
                                <label class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-7">
                                    <input id="styled-finputs-example" type="file" name="userfile" class="form-control" placeholder="Picture" >
                                    <?php echo form_error('userfile', '<span class="help-block"><i class="fa fa-warning"></i> ', '</span>'); ?>
                                </div>
                            </div>
							<div class="row">
								<div class="col-sm-3 col-sm-offset-2">
									<img width="100%" class="img img-responsive" src="<?php echo base_url()?>uploads/user/<?php echo set_value('user_picture', isset($default['user_picture']) ? $default['user_picture'] : 'f3.png'); ?>">
								</div>
							</div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Save</button>
                        <a href="<?php echo base_url()?>user_admin"><button class="btn btn-danger" type="button">Back</button></a>
                    </div>
                </form>
                
<!-- /5. $SUMMERNOTE_WYSIWYG_EDITOR -->
    </div> <!-- / #content-wrapper -->
    <div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->
<?php $this->load->view('admin/page_form_script_below'); ?>