<div class="sidebar">
	<div class="related_post_sec">
		<div class="list_block">
			<h3>Academic</h3>
			<ul style="display: table;width: 100%">
				<li><a href="<?=base_url('our-school/gp-montessori') ?>">Gp Montessori</a></li>
				<li><a href="<?=base_url('our-school/gp-elementary') ?>">GP Elementary</a></li>
				<li><a href="<?=base_url('our-school/gp-jhs') ?>">GP Junior High School</a></li>
				<li><a href="<?=base_url('our-school/gp-shs') ?>">GP Senior High School</a></li>
				<li><a href="<?=base_url('our-school/harapan-mulia') ?>">Harapan Mulia</a></li>
			</ul>
		</div>
	</div><!--end sidebar item-->

	<div class="sidebar_item admission">
		<div class="item_inner program">
			<ul>
				<li><a href="<?=base_url('about-gps/history') ?>">History, Vision Mission, Philosophy</a></li>
				<li><a href="<?=base_url('about-gps/excelence-program') ?>">Excelence Program</a></li>
				<li><a href="<?=base_url('about-gps/facilities') ?>">Facilities</a></li>
				<li><a href="<?=base_url('about-gps/mars-gps') ?>">Mars GPS</a></li>
				<li><a href="<?=base_url('contact-us') ?>">Contact Us</a></li>
			</ul>
		</div>
	</div><!--end sidebar item-->

	<div class="related_post_sec">
		<div class="list_block">
			<h3>Latest News</h3>
			<ul>
				<?php foreach($news as $key => $row){ ?>
				<li>
					<span class="rel_thumb">
						<img src="<?=upload_url('blog/'.$row->blog_picture) ?>" alt="">
					</span><!--end rel_thumb-->
					<div class="rel_right">
						<a href="<?=base_url('blog/'.$row->blog_slug) ?>"><h4><?=character_limiter($row->blog_title, 45) ?></h4></a>
						<span class="date">Posted: <a href="<?=base_url('blog/'.$row->blog_slug) ?>">January 24, 2015</a></span>
					</div><!--end rel right-->
				</li>
				<?php } ?>
			</ul>
			<a href="<?=base_url('blog') ?>" class="more_post">More</a>
		</div>
	</div>
</div><!--end sidebar-->