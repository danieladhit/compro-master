<div class="custom_content custom">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-8 custom_right">
				<div class="single_content_left default-list-style">
					<?php $this->load->view('_partials/breadcrumb'); ?>
					<h3>Admission</h3>
					<br>
					<?=alert_box() ?>
					<h4>REGISTRATION INFO</h4>
					<button class="btn btn-primary btn-block" onclick="$('#registrationInfoModal').modal('show')">Click to view Registration Info</button>
					<br>
					<br>
					<div class="contact-clean">
						<form method="post" action="<?=base_url('admission/save_admission') ?>">
							<h4 class="text-left text-uppercase">Online Registration</h4>
							<button type="button" class="btn btn-primary btn-block" onclick="$('#onlineRegistrationInfoModal').modal('show')">Click to view Online Registration Info</button>
							<div class="form-group has-feedback">
								<label for="">Name</label>
								<input class="form-control" type="text" name="name" placeholder="Name" /><i class="form-control-feedback" aria-hidden="true"></i>
							</div>
							<div class="form-group has-feedback">
								<label for="email">Email</label>
								<input class="form-control" type="email" name="email" placeholder="Email" /><i class="form-control-feedback" aria-hidden="true"></i>
							</div>
							<div class="form-group has-feedback">
								<label for="phone">Phone Number</label>
								<input class="form-control" type="number" name="phone" placeholder="Phone" /><i class="form-control-feedback" aria-hidden="true"></i>
							</div>
							<div class="form-group has-feedback">
								<label for="grade">Which Grade are you interested in?</label>
								<select name="grade" id="grade" class="form-control">
									<option value="montessori">GP Montessori</option>
									<option value="elementary">GP Elementary</option>
									<option value="jhs">GP Junior High School</option>
									<option value="shs">GP Senior High School</option>
								</select>
							</div>
							<div class="form-group has-feedback">
								<label for="early">Mini Open Campus Date</label>
								<input type="text" name="early" id="early" class="form-control">
							</div>
							<div class="form-group has-feedback">
								<label for="language">Preferred Language</label>
								<select name="language" id="language" class="form-control">
									<option value="indonesian">Indonesian</option>
									<option value="english">English</option>
								</select>
							</div>
							<div class="form-group">
								<label for="message">Additional Information</label>
								<textarea class="form-control" rows="14" name="message" placeholder="Additional Information"></textarea>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">send </button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 custom_left">
				<?php include "sidebar.php"; ?>
			</div>
		</div><!--end row-->
	</div>
</div>

<div class="modal fade" id="registrationInfoModalImage" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">REGISTRATION INFO</h4></div>
				<div class="modal-body" style="padding: 40px;">
					<img src="src/images/register-info.jpg" class="img-responsive" width="100%" alt="">
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="registrationInfoModal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">REGISTRATION INFO</h4></div>
				<div class="modal-body" style="padding: 40px;">
					<h5>For School Registration</h5>
					<ol>
						<li>Fill the application form</li>
						<li>Document required :
							<ol style="list-style: disc;">
								<li>Copy of Birth Certificate (akte kelahiran)</li>
								<li>Copy of Family Card (kartu keluarga)</li>
								<li>Copy of Previous School's Report Card (fotokopi raport)</li>
								<li>4x6 color photos & 3x4 color photos (2 pcs)</li>
								<li>Copy of Parents ID (fotokopi KTP)</li>
							</ol>
						</li>
						<li>Applicant will need to undergo Global Prestasi Placement test.</li>
					</ol>

					<h5>Test Subject (General) : </h5>
					<ol>
						<li>Elementary : Math & Reading</li>
						<li>Junior High : English & Math</li>
						<li>Senior High : Math, English & Computer</li>
					</ol>

					<h5>Test Subject for International Program : </h5>
					<p>English, Math, Interview and Asessment</p>

					<h5>Contact us for more details</h5>
					<ol>
						<li>WhatsApp : 0819 0888 3385</li>
						<li>Admission Info/new student enrollment : (021) 885 4311</li>
						<li>Operator : (021) 8885 2668</li>
					</ol>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="onlineRegistrationInfoModal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">ONLINE REGISTRATION INFO</h4></div>
				<div class="modal-body" style="padding: 40px;">
					<p><strong>Step 1: Download the registration form in the following</strong></p>
					<?php $base_url = "http://globalprestasi.sch.id"; ?>
					<ol style="padding-left: 40px;">
						<li><a target="_blank" href="<?=$base_url ?>/sites/default/files/FORM%20MONTESS.pdf">Montessori</a></li>
						<li><a target="_blank" href="<?=$base_url ?>/sites/default/files/FORM%20SD.pdf">Elementary</a></li>
						<li><a target="_blank" href="<?=$base_url ?>/sites/default/files/FORM%20SMP.pdf">Junior High</a></li>
						<li><a target="_blank" href="<?=$base_url ?>/sites/default/files/FORM%20SMA.pdf">Senior High</a></li>
					</ol>
					<p><strong>Step 2: Fill the registration form</strong></p>
					<p><strong>Step 3: Transfer the registration form fee of IDR 600,000*) to the following account:</strong></p>
					<p style="margin: 0">Account holder name: Yay. Harapan Global Mandiri</p>
					<p style="margin: 0">Account number: 065 3099158</p>
					<p>Bank's name: BCA Kcu Kelapa Gading</p>
					<p><em>Please confirm for the special early bird form fee by phone to <strong>8854311</strong> before processing the fund transfer.</em></p>
					<p><strong>Step 4: Send the transfer proof and completed registration form via email or Whatsapp, and make an appointment for the placement test.</strong></p>
					<p>Email :</p>
					<ol style="padding-left: 40px;">
						<li>stephanie.widjaya@globalprestasi.sch.id </li>
						<li>yessica.sitohang@globalprestasi.sch.id </li>
						<li>theresiasabono@globalprestasi.sch.id</li>
					</ol>
					<p>WhatsApp : <strong>0819 0888 3385</strong></p>
					<p>Phone : <strong>(021) 8885 2668 / 885 4311</strong></p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
