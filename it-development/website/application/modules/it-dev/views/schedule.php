<div class="row">
  <div class="col-md-3">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h4 class="box-title">Rooms</h4>

        <div class="box-tools">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
          <?php foreach ($rooms as $key => $row){ ?>
          <li class="<?=$room == $row->name ? 'active' : '' ?>"><a href="<?=base_url('it-dev/schedule/index?room_id='.$row->room_id.'&name='.$row->name) ?>"><?=$row->name ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /. box -->
    <div class="box box-solid">
      <div class="box-header with-border">
        <h4 class="box-title">Draggable Events</h4>
      </div>
      <div class="box-body">
        <!-- the events -->
        <div id="external-events">
          <!-- <div class="external-event bg-green">Rapat Manajemen</div>
          <div class="external-event bg-yellow">Rapat HRD</div>
          <div class="external-event bg-aqua">Rapat IT</div> -->
          <?php foreach($events_draggable as $key => $event){ ?>
          <div class="external-event" data-id="<?=$event->event_drag_id ?>" style="background-color: <?=$event->backgroundColor ?>; color: <?=$event->color ?>"><?=$event->label ?></div>
          <?php } ?>
          <div class="checkbox" style="display: block">
            <label for="drop-remove">
              <input type="checkbox" id="drop-remove">
              remove after drop
            </label>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /. box -->
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Create Event</h3>
      </div>
      <div class="box-body">
        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
          <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
          <ul class="fc-color-picker" id="color-chooser">
            <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
          </ul>
        </div>
        <!-- /btn-group -->
        <div class="input-group">
          <input id="new-event" type="text" class="form-control" placeholder="Event Title">

          <div class="input-group-btn">
            <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
          </div>
          <!-- /btn-group -->
        </div>
        <!-- /input-group -->
      </div>
    </div>
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <?=alert_box() ?>
    <?php if ($this->room_id != NULL) { ?>
    <div class="box box-primary">
      <div class="box-body no-padding">
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /. box -->
    <?php } else { ?>
    <div class="alert alert-info">
      Silahkan pilih Ruangan terlebih dahulu
    </div> 
    <?php  } ?>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->


<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00a65a; color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Calendar Event</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open(base_url("it-dev/schedule/add_event"), array("class" => "form-horizontal")) ?>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">Event Name</label>
          <div class="col-md-8 ui-front">
            <input id="name" type="text" class="form-control" name="name" value="" required="required">
          </div>
        </div>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">PIC</label>
          <div class="col-md-8 ui-front">
            <input id="pic" type="text" class="form-control" name="pic" value="" required="required">
          </div>
        </div>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">Participant</label>
          <div class="col-md-8 ui-front">
            <select id="participant" class="form-control select2" name="participant[]" multiple="multiple" data-placeholder="Select Participant"
            style="width: 100%">
            <?php foreach($participant as $key => $pa){ ?>
            <option value="<?=$pa->email ?>"><?=$pa->name ?></option>
            <?php } ?>
          </select>
          <input type="hidden" id="participant_email" name="participant_email">
        </div>
      </div>
      <div class="form-group">
        <label for="p-in" class="col-md-4 label-heading">Description / Equipment</label>
        <div class="col-md-8 ui-front">
          <select id="equipment" class="form-control select2" name="equipment[]" multiple="multiple" data-placeholder="Select Equipment"
          style="width: 100%">
          <?php foreach($equipment as $key => $eq){ ?>
          <option><?=$eq->name ?></option>
          <?php } ?>
        </select>
        <textarea style="margin-top: 20px;" name="description" class="form-control" id="description" cols="30" rows="5"></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="p-in" class="col-md-4 label-heading">Date</label>
      <div class="col-md-8">
        <input id="start" type="text" class="form-control datepicker" name="start_date" required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="p-in" class="col-md-4 label-heading">Time</label>
      <div class="col-md-8">
        <input id="time" type="text" class="form-control timepicker" name="time" required="required">
      </div>
    </div>
    <div class="form-group">
      <label for="p-in" class="col-md-4 label-heading">Duration</label>
      <div class="col-md-8">
        <div class="row">
          <div class="col-sm-6 col-md-6">
            <label class="visible-sm"><strong>Hour : </strong></label>
            <select name="hour" id="hour">
              <option value="+1 hour">1 Hour</option>
              <option value="+2 hour">2 Hour</option>
              <option value="+3 hour">3 Hour</option>
              <option value="+4 hour">4 Hour</option>
              <option value="+5 hour">5 Hour</option>
              <option value="+6 hour">6 Hour</option>
            </select>
          </div>
          <div class="col-sm-6 col-md-6">
           <label class="visible-sm"><strong>Minute : </strong></label>
           <select name="minute" id="minute">
            <option value="">-- Choose Minute --</option>
            <option value="+15 minutes">15 Minutes</option>
            <option value="+30 minutes">30 Minutes</option>
            <option value="+45 minutes">45 Minutes</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="label-heading col-md-4">Label Color</label>
    <div class="col-md-8">
      <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
        <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
        <ul class="fc-color-picker" id="color-chooser-modal">
          <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
          <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <input type="hidden" name="backgroundColor" id="backgroundColor" value="#00a65a">
  <input type="hidden" name="borderColor" id="borderColor" value="#00a65a">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <input id="btn-submit" type="submit" class="btn" value="Add Event" style="background-color: #00a65a; color: #fff;">
  <?php echo form_close() ?>
</div>
</div>
</div>
</div>


<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="color: #fff">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Calendar Event</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open(site_url("it-dev/schedule/edit_event"), array("class" => "form-horizontal")) ?>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">Event Name</label>
          <div class="col-md-8 ui-front">
            <input type="text" class="form-control" name="name" value="" id="name">
          </div>
        </div>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">PIC</label>
          <div class="col-md-8 ui-front">
            <input id="pic" type="text" class="form-control" name="pic" value="" required="required">
          </div>
        </div>
         <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">Participant</label>
          <div class="col-md-8 ui-front">
            <select id="participant" class="form-control select2" name="participant[]" multiple="multiple" data-placeholder="Select Participant"
            style="width: 100%">
            <?php foreach($participant as $key => $pa){ ?>
            <option value="<?=$pa->email ?>"><?=$pa->name ?></option>
            <?php } ?>
          </select>
          <input type="hidden" id="participant_email2" name="participant_email">
        </div>
      </div>
        <div class="form-group">
          <label for="p-in" class="col-md-4 label-heading">Description / Equipment</label>
          <div class="col-md-8 ui-front">
            <select id="equipment" class="form-control select2" id="equipment " name="equipment[]" multiple="multiple" data-placeholder="Select Equipment"
            style="width: 100%">
            <?php foreach($equipment as $key => $eq){ ?>
            <option><?=$eq->name ?></option>
            <?php } ?>
          </select>
          <textarea style="margin-top: 20px;" name="description" class="form-control" id="description" cols="30" rows="5"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="p-in" class="col-md-4 label-heading">Date</label>
        <div class="col-md-8">
          <input id="start" type="text" class="form-control datepicker" name="start_date" required="required">
        </div>
      </div>
      <div class="form-group">
        <label for="p-in" class="col-md-4 label-heading">Time</label>
        <div class="col-md-8">
          <input id="time" type="text" class="form-control timepicker" name="time" required="required">
        </div>
      </div>
      <div class="form-group">
        <label for="p-in" class="col-md-4 label-heading">Duration</label>
        <div class="col-md-8">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <label class="visible-sm"><strong>Hour : </strong></label>
              <select name="hour" id="hour">
                <option selected="" value="+1 hour">1 Hour</option>
                <option value="+2 hour">2 Hour</option>
                <option value="+3 hour">3 Hour</option>
                <option value="+4 hour">4 Hour</option>
                <option value="+5 hour">5 Hour</option>
                <option value="+6 hour">6 Hour</option>
              </select>
            </div>
            <div class="col-sm-6 col-md-6">
             <label class="visible-sm"><strong>Minute : </strong></label>
             <select name="minute" id="minute">
              <option value="">-- Choose Minute --</option>
              <option value="+15 minutes">15 Minutes</option>
              <option value="+30 minutes">30 Minutes</option>
              <option value="+45 minutes">45 Minutes</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="label-heading col-md-4">Label Color</label>
      <div class="col-md-8">
        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
          <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
          <ul class="fc-color-picker" id="color-chooser-modal">
            <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="p-in" class="col-md-4 label-heading">Delete Event</label>
      <div class="col-md-8">
        <input id="event-remove" type="checkbox" name="delete" value="1"> <em>*Delete this schedule from calendar</em>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <input type="hidden" name="eventid" id="event_id" value="0" />
    <input type="hidden" name="backgroundColor" id="backgroundColor">
    <input type="hidden" name="borderColor" id="borderColor">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <input id="btn-submit" type="submit" class="btn btn-primary btn-update-submit" value="Update Event">
    <?php echo form_close() ?>
  </div>
</div>
</div>
</div>