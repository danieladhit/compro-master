<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('calendar_model', 'calendar');
		$this->load->model('global_model', 'global');

		$this->get_room_session();

		// get user group
		$this->user = $this->ion_auth->get_users_groups()->row();

		// get user session
		$this->user_session = $this->ion_auth->user()->row();
	}

	public function index()
	{
		if ( $this->input->get('room_id') )
		{
			$room_id = $this->input->get('room_id');
			$room_name = $this->input->get('name');

			$this->session->set_userdata('room', array(
				'room_id' => $room_id,
				'room_name' => $room_name
				));

			$this->get_room_session();
		}

		$room_name = $this->room_name == NULL ? '' : "( $this->room_name )";

		$this->mPageTitle = 'Meeting Calendar '.$room_name;
		$this->mViewData['events_draggable'] = $this->global->get_data('calendar_events_draggable');
		$this->mViewData['equipment'] = $this->global->get_data('calendar_equipment');
		$this->mViewData['participant'] = $this->global->get_data('calendar_participant');
		$this->mViewData['rooms'] = $this->global->get_data('calendar_rooms');
		$this->mViewData['room'] = $this->room_name;
		$this->render('schedule');
	}

	private function get_room_session()
	{
		$room_session = $this->session->userdata('room');

		if ( count($room_session) > 0 )
		{
			$this->room_id = $room_session['room_id'];
			$this->room_name = $room_session['room_name'];
		}
		else
		{
			$this->room_id = NULL;
			$this->room_name = NULL;
		}
	}

	private function save_admin_log($activity,$json)
	{
		$data['create_date'] = date('Y-m-d H:i:s');
		$data['activity'] = $activity;
		$data['data'] = $json;
		$data['admin_id'] = $this->user->id;
		$this->db->insert('log_admin', $data);
	}

	public function get_events()
	{
		// Our Start and End Dates
		$start = $this->input->get("start");
		$end = $this->input->get("end");


	    $startdt = new DateTime('now'); // setup a local datetime
	    $startdt->setTimestamp($start); // Set the date based on timestamp
	    $start_format = $startdt->format('Y-m-d H:i:s');

	    $enddt = new DateTime('now'); // setup a local datetime
	    $enddt->setTimestamp($end); // Set the date based on timestamp
	    $end_format = $enddt->format('Y-m-d H:i:s');

	    $this->db->where('room_id', $this->room_id);
	    $events = $this->calendar->get_events($start_format, $end_format);


	    $data_events = array();

	    foreach($events->result() as $r) {
	    	$equipment_array = explode("#eq#", $r->equipment);
	    	$participant_array = explode("#pr#", $r->participant);
	    	$participant_email_array = explode(",", $r->participant_email);

	    	$data_events[] = array(
	    		"id" => $r->event_id,
	    		"admin_id" => $r->admin_id,
	    		"room_id"	=> $this->room_id,
	    		"title" => $r->title,
	    		"pic" => $r->pic,
	    		"participant" => $participant_array,
	    		"participant_email" => $participant_email_array,
	    		"equipment" => $equipment_array,
	    		"description" => $r->description,
	    		"end" => $r->end,
	    		"start" => $r->start,
	    		"backgroundColor" => $r->backgroundColor,
	    		"borderColor" => $r->borderColor,
	    		);
	    }

	    $this->save_admin_log('Get Events', '');

	    echo json_encode(array("events" => $data_events));
	    exit();
	}

	private function validate_return($json)
	{
		if ($json) 
		{
			echo json_encode(array('status' => 0));
			exit();
		}
		else
		{
			redirect_referrer();
		}
	}

	private function validate_event($start, $end, $event_id = 0,$json = FALSE)
	{
		$batas_awal = date('H:i', strtotime('08:00'));
		$batas_akhir = date('H:i', strtotime('16:00'));

		$jam_awal = date('H:i', strtotime($start));
		$jam_akhir = date('H:i', strtotime($end));

		if ($jam_awal < $batas_awal || $jam_awal > $batas_akhir)
		{
			set_alert("danger", "Maaf !! Waktu booking adalah pukul $batas_awal s/d $batas_akhir");
			$this->validate_return($json);
		}

		$this->db->where('event_id != ', $event_id);
		$this->db->where("room_id", $this->room_id);

		$this->db->group_start();
		$this->db->where("start BETWEEN '$start' AND '$end' ");
		$this->db->or_where("end BETWEEN '$start' AND '$end' ");
		$this->db->group_end();
		
		$get_events = $this->global->get_data('calendar_events');
		//print_r($get_events); exit();

		if ( count($get_events) > 0 )
		{
			$event = $get_events[0];
			set_alert('danger', "Maaf !! Ruangan telah dibooking untuk $event->title pada tanggal ".date('d F Y H:i', strtotime($event->start))." - ".date('H:i' , strtotime($event->end)));
			$this->validate_return($json);
		}
	}

	private function validate_permission($id_admin_bersangkutan, $json = FALSE)
	{
		$id_admin_login = $this->user_session->id;
		$permission = FALSE;
		$admin_role = $this->user->name;

		if ($id_admin_login == $id_admin_bersangkutan || $admin_role == 'webmaster' || $admin_role == 'superadmin')
		{
			$permission = TRUE;
		}

		if ($permission == FALSE)
		{
			set_alert("danger", "You don't have permissions to change this event");
			
			if ($json)
			{
				echo json_encode( array("message" => "You don't have permissions to change this event", "status" => 0, "data" => $this->input->post()) );
				exit();
			}

			
			redirect(site_url("it-dev/schedule"));
		}


		return true;
	}

	public function add_event() 
	{
		/* Our calendar data */
		$name = $this->input->post("name", TRUE);
		$pic = $this->input->post("pic", TRUE);
		$equipment = implode('#eq#', $this->input->post("equipment", TRUE));

		$participant_email_array = $this->input->post("participant", TRUE);
		$participant_email = implode(',', $participant_email_array);

		$get_participant_name = $this->db->where_in('email', $participant_email_array)->get('calendar_participant')->result();

		foreach ($get_participant_name as $row) {
			$participant_array[] = $row->name;
		}

		$participant = implode('#pr#', $participant_array);

		$desc = $this->input->post("description", TRUE);
		$start_date = $this->input->post("start_date", TRUE);
		$time = $this->input->post("time", TRUE);
		$hour = $this->input->post("hour", TRUE);
		$minute = $this->input->post("minute", TRUE);
		$backgroundColor = $this->input->post("backgroundColor", TRUE);
		$borderColor = $this->input->post("borderColor", TRUE);

		if(!empty($start_date)) {
			$start_date_time = date('Y-m-d H:i:s', strtotime($start_date.' '.$time));
		} else {
			$start_date_time = date("Y-m-d H:i:s", time());
		}

		if(!empty($time)) {
			$end_date_time = date('Y-m-d H:i:s', strtotime($start_date_time.$hour.$minute));
		} else {
			$end_date_time = date("Y-m-d H:i:s", strtotime($start_date_time.'1 hour'));
		}

		// validate event
		$this->validate_event($start_date_time, $end_date_time);

		$event_data = array(
			"room_id" => $this->room_id,
			"admin_id"	=> $this->user_session->id,
			"title" => $name,
			"pic" => $pic,
			"participant" => $participant,
			"participant_email" => $participant_email,
			"equipment" => $equipment,
			"description" => $desc,
			"start" => $start_date_time,
			"end" => $end_date_time,
			"backgroundColor" => $backgroundColor,
			"borderColor" => $borderColor
			);

		// save event to database
		$this->calendar->add_event($event_data);

		// save to admin log
		$json = json_encode($event_data);
		$this->save_admin_log('Add Event', $json);

		// send invitation email to participant
		$email_data['data'] = array(
			'Subject'	=> $name,
			'PIC'		=> $pic,
			'Location'	=> $this->room_name,
			'Participant' => implode(', ', $participant_array),
			'Start'		=> date('d F Y H:i:s', strtotime($start_date_time) ),
			'End'		=> date('d F Y H:i:s', strtotime( $end_date_time) ),
			'Description' => $desc,
			'Equipment'	=> $equipment,
			);

		$email_data['email_info'] = array(
			'from_name' => 'info@globalprestasi.sch.id'
			);
		//$participant_email_array = array('adhityakristy@gmail.com', 'jhedhot@gmail.com', 'daniel.adhitya@globalprestasi.sch.id');

		send_email($participant_email_array, 'Meeting Participant', 'Meeting Invitation', 'general',$email_data);

		set_alert("success", "Event Schedule $name pada tanggal $start_date_time berhasil ditambah");

		redirect(site_url("it-dev/schedule"));
	}

	public function edit_event()
	{
		$eventid = intval($this->input->post("eventid"));
		$event = $this->calendar->get_event($eventid);
		if($event->num_rows() == 0) {
			echo"Invalid Event";
			exit();
		}

		$event_data = $event->row();

		// permission validate
		$this->validate_permission($event_data->admin_id);

		/* Our calendar data */
		$name = $this->input->post("name", TRUE);
		$pic = $this->input->post("pic", TRUE);
		
		$participant_email_array = $this->input->post("participant", TRUE);
		$participant_email = implode(',', $participant_email_array);

		$get_participant_name = $this->db->where_in('email', $participant_email_array)->get('calendar_participant')->result();

		foreach ($get_participant_name as $row) {
			$participant_array[] = $row->name;
		}

		$participant = implode('#pr#', $participant_array);

		$equipment = implode('#eq#', $this->input->post("equipment", TRUE));
		$desc = $this->input->post("description", TRUE);
		$start_date = $this->input->post("start_date", TRUE);
		$time = $this->input->post("time", TRUE);
		$hour = $this->input->post("hour", TRUE);
		$minute = $this->input->post("minute", TRUE);
		$backgroundColor = $this->input->post("backgroundColor", TRUE);
		$borderColor = $this->input->post("borderColor", TRUE);
		$delete = intval($this->input->post("delete") );

		if(!empty($start_date)) {
			$start_date_time = date('Y-m-d H:i:s', strtotime($start_date.' '.$time));
		} else {
			$start_date_time = date("Y-m-d H:i:s", time());
		}

		if(!empty($time)) {
			$end_date_time = date('Y-m-d H:i:s', strtotime($start_date_time.$hour.$minute));
		} else {
			$end_date_time = date("Y-m-d H:i:s", strtotime($start_date_time.'1 hour'));
		}

		$event_data = array(
			"room_id" => $this->room_id,
			"title" => $name,
			"pic" => $pic,
			"participant" => $participant,
			"participant_email" => $participant_email,
			"equipment" => $equipment,
			"description" => $desc,
			"start" => $start_date_time,
			"end" => $end_date_time,
			"backgroundColor" => $backgroundColor,
			"borderColor" => $borderColor
			);

		if(!$delete) {

			// validate event
			$this->validate_event($start_date_time, $end_date_time, $eventid);

			$this->calendar->update_event($eventid, $event_data);

			// save to log activity
			$json = json_encode(array(
				'before' => $event->row(),
				'after' => $event_data
				));
			$this->save_admin_log('Edit Event', $json);

			// send invitation email to participant
			$email_data['data'] = array(
				'Subject'	=> $name,
				'PIC'		=> $pic,
				'Location'	=> $this->room_name,
				'Participant' => implode(', ', $participant_array),
				'Start'		=> date('d F Y H:i:s', strtotime($start_date_time) ),
				'End'		=> date('d F Y H:i:s', strtotime( $end_date_time) ),
				'Description' => $desc,
				'Equipment'	=> $equipment,
				);

			$email_data['email_info'] = array(
				'from_name' => 'info@globalprestasi.sch.id'
				);
			//$participant_email_array = array('adhityakristy@gmail.com', 'jhedhot@gmail.com', 'daniel.adhitya@globalprestasi.sch.id');

			send_email($participant_email_array, 'Meeting Participant', 'Meeting Invitation', 'general',$email_data);

			// set success alert
			set_alert("success", "Event Schedule $name pada tanggal $start_date_time berhasil diupdate");

		} else {
			$this->calendar->delete_event($eventid);

			// save to log activity
			$json = json_encode($event_data);
			$this->save_admin_log('Delete Event', $json);

			set_alert("info", "Event Schedule $name pada tanggal $start_date_time berhasil dihapus");
			redirect(site_url("it-dev/schedule"));
		}

		redirect(site_url("it-dev/schedule"));

	}

	public function edit_drop_event()
	{
		$eventid = intval($this->input->post("eventid"));
		$event = $this->calendar->get_event($eventid);
		if($event->num_rows() == 0) {
			echo json_encode( array("message" => "Invalid Event", "status" => 0) );
			exit();
		}

		$event_data = $event->row();

		// permission validate
		$this->validate_permission($event_data->admin_id, TRUE);

		$start = $this->input->post("start");
		$end = $this->input->post("end");

		$start_date_time = date('Y-m-d H:i:s', strtotime($start) );
		$end_date_time = date('Y-m-d H:i:s', strtotime($end) );

		// validate event
		$this->validate_event($start_date_time, $end_date_time, $eventid, TRUE);

		$event_data = array(
			"start" => $start_date_time,
			"end" => $end_date_time 
			);

		$this->calendar->update_event($eventid, $event_data);

		// save to log activity
		$json = json_encode(array(
			'before' => $event->row(),
			'after' => $event_data
			));
		$this->save_admin_log('Edit Event With Drop', $json);

		echo json_encode( array("message" => "Success !!", "status" => 1, "data" => $this->input->post()) );
		exit();

	}

	public function add_draggable_event()
	{
		if ( !$this->input->post() )
		{
			echo 'You are not authorized';
			exit();
		}

		$event_data = array(
			'event_drag_id'		=>	NULL,
			'label'				=>	$this->input->post('label', TRUE),
			'backgroundColor'	=>	$this->input->post('backgroundColor', TRUE),
			'color'				=>	$this->input->post('color', TRUE)
			);

		$process = $this->db->insert('calendar_events_draggable', $event_data);

		// save to log activity
		$json = json_encode($event_data);
		$this->save_admin_log('Add Dragable Event', $json);

		echo json_encode(array('status' => $process));

		exit();
	}

	public function remove_draggable_event()
	{
		if ( !$this->input->post() )
		{
			echo 'You are not authorized';
			exit();
		}

		$event_drag_id = $this->input->post('event_drag_id', TRUE);

		$event_data = $this->global->get_data('calendar_events_draggable', ['event_drag_id' => $event_drag_id], TRUE);

		$process = $this->db->delete('calendar_events_draggable', ['event_drag_id' => $event_drag_id]);

		// save to log activity
		$json = json_encode($event_data);
		$this->save_admin_log('Remove Dragable Event', $json);

		echo json_encode(array('status' => $process));

		exit();
	}

}
