<!-- ======== @Region: #footer ======== -->
<footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="img/bg_footer-map.png" data-stellar-background-ratio="0.4">
  <div class="container">

    <div class="row" id="contact">

      <div class="col-md-3 col-sm-4">
        <address>
          <strong>Global Prestasi Montessori</strong>
          <br>
          <i class="fa fa-map-pin fa-fw text-primary"></i> Komplek GPS Jl.K.H Noer Alie No.10 B
Bekasi Jawa Barat 17145
          <br>
          <i class="fa fa-phone fa-fw text-primary"></i> 021-888 52668
          <br>
          <i class="fa fa-whatsapp fa-fw text-primary"></i> 0819 0888 3385
          <br>
          <i class="fa fa-envelope-o fa-fw text-primary"></i> info@gobalprestasi.sch.id
          <br>
        </address>
      </div>

      <div class="col-md-3 col-sm-4">
         <address>
          <strong>Harapan Mulia Elementary School</strong>
          <br>
          <i class="fa fa-map-pin fa-fw text-primary"></i> Jl.Gardenia 1 Kav 21 -24 Villa Galaxy - Bekasi
          <br>
          <i class="fa fa-phone fa-fw text-primary"></i> 021-824 02776
          <br>
          <i class="fa fa-envelope-o fa-fw text-primary"></i> info@gobalprestasi.sch.id
        </address>
      </div>

      <div class="col-md-3 col-sm-4">
         <address>
          <strong>Global Prestasi Kids</strong>
          <br>
          <i class="fa fa-map-pin fa-fw text-primary"></i> Pondok Kelapa Indah B2/1 Jakarta Timur
13810
          <br>
          <i class="fa fa-phone fa-fw text-primary"></i> 021-864 1603
          <br>
          <i class="fa fa-envelope-o fa-fw text-primary"></i> info@globalprestasi.sch.id
          <br>
        </address>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-3 col-sm-6">
        <h4 class="text-uppercase">
          Follow Us On:
        </h4>
        <!--social media icons-->
        <div class="social-media social-media-stacked">
          <!--@todo: replace with company social media details-->
          <a href="#"><i class="fa fa-twitter fa-fw"></i> Twitter</a>
          <a href="#"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
          <a href="#"><i class="fa fa-google-plus fa-fw"></i> Google+</a>
        </div>
      </div>

    </div>

    <div class="row subfooter">
      <div class="col-md-7">
        <p>Copyright © Global Prestasi School</p>
      </div>
      <div class="col-md-5">
        <ul class="list-inline pull-right">
          
        </ul>
      </div>
    </div>

    <a href="#top" class="scrolltop">Top</a> 

  </div>
</footer>


</body>
</html>